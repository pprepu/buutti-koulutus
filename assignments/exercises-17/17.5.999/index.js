import express from 'express'
import { executeQuery } from './db/db.js'
const findOneMessage = 'SELECT id, text FROM messages;';

const server = express()

const PORT = process.env.port || 3000

server.get('/message', async (req, res) => {
  try {
    const dbResponse = await executeQuery(findOneMessage)
    return res.send(dbResponse.rows[0].text)
  } catch (error) {
    return res.status(401).send('error')
  }
})

server.listen(PORT, () => {
  console.log('listening to port', PORT)
//   console.log(process.env.PG_PORT)
//   console.log(process.env.PG_HOST)
//   console.log(process.env.PG_USERNAME)
//   console.log(process.env.PG_PASSWORD)
})