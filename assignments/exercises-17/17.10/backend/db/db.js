import pg from 'pg'

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env

const isInProduction = process.env.NODE_ENV === 'production'

const pool = new pg.Pool({
  host: PG_HOST,
  port: PG_PORT,
  user: PG_USERNAME,
  password: PG_PASSWORD,
  database: PG_DATABASE,
  ssl: isInProduction
})

export const executeQuery = async (query, parameters) => {
  const client = await pool.connect()
  try {
    const queryResult = await client.query(query, parameters)
    return queryResult
  } catch (error) {
    console.log(error)
    throw error
  } finally {
    client.release()
  }
}