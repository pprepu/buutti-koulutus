import { executeQuery } from "./db.js";
import queries from './queries.js'

const findAll = async () => {
  const dbResponse = await executeQuery(queries.findAll)
  return dbResponse.rows
}

const findOne = async id => {
  const dbResponse = await executeQuery(queries.findOne, [id])
  // if id doesn't exist, this function returns a nonexisting index from an empty array (which results in undefined) - weird
  return dbResponse.rows[0]
}

const createSong = async (title, lyrics) => {
  const dbResponse = await executeQuery(queries.createSong, [title, lyrics])
  return dbResponse.rows[0]
}

const deleteSong = async id => {
  const dbResponse = await executeQuery(queries.deleteSong, [id])
  return dbResponse.rowCount
}

const updateSong = async (title, lyrics, id) => {
  const dbResponse = await executeQuery(queries.updateSong, [title, lyrics, id])
  return dbResponse.rows[0]
}

export default {
  findAll,
  findOne,
  createSong,
  deleteSong,
  updateSong
}