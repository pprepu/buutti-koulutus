const findAll = "SELECT id, title, lyrics FROM songs;";

const findOne = "SELECT id, title, lyrics FROM songs WHERE id = $1;"

const createSong = "INSERT INTO songs (title, lyrics) VALUES ($1, $2) RETURNING id;"

const deleteSong = "DELETE from songs WHERE id = $1;"

const updateSong = "UPDATE songs SET title = $1, lyrics = $2 WHERE id = $3 RETURNING id;"

export default {
  findAll,
  findOne,
  createSong,
  deleteSong,
  updateSong
}