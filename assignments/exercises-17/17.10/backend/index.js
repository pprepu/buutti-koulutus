import express from 'express'
import cors from 'cors'
import url from 'url'
import path from 'path'
import dao from './db/songDao.js'

import { validateFields } from './middlewares.js'

const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url))
const distDirectory = path.resolve(currentDirectory, './dist/')

const PORT = process.env.PORT || 3000
const server = express()
server.use(cors())
server.use(express.json())
server.use(express.static(distDirectory))

server.get('/songs', async (req, res) => {
  const songsInDb = await dao.findAll()
  res.send(songsInDb)
})

server.get('/songs/:id', async (req, res) => {
  const id = Number(req.params.id)

  const songFound = await dao.findOne(id)

  if (!songFound) {
    return res.status(404).send('could not find song with given id')
  }
  res.send(songFound)
})

server.post('/songs', validateFields, async (req, res) => {
  const { title, lyrics } = req.body

  const songIdInObject = await dao.createSong(title, lyrics)
  if (!songIdInObject) {
    return res.status(406).send('could not create song')
  }
  const newSong = {
    id: songIdInObject.id,
    title,
    lyrics
  }
  res.status(201).send(newSong)
})

server.delete('/songs/:id', async (req, res) => {
  const id = Number(req.params.id)

  const rowsAffectedInDb = await dao.deleteSong(id)

  if (rowsAffectedInDb === 0) {
    return res.status(404).send('could not find song with given id')
  }

  res.status(204).send()
})

server.put('/songs/:id', validateFields, async (req, res) => {
  const id = Number(req.params.id)

  const { title, lyrics } = req.body

  const songIdInObject = await dao.updateSong(title, lyrics, id)
  if (!songIdInObject) {
    return res.status(404).send('could not find song with given id')
  }

  const updatedSong = {
    id: songIdInObject.id,
    title,
    lyrics
  }

  res.status(201).send(updatedSong)
})

server.get('*', (req, res) => {
  res.sendFile('index.html', { root: distDirectory })
})


server.listen(PORT, () => {
  console.log('listening to port', PORT)
  console.log('node env', process.env.NODE_ENV)
  if (process.env.NODE_ENV === 'development') {
    console.log(process.env.PG_PORT)
    console.log(process.env.PG_HOST)
    console.log(process.env.PG_USERNAME)
    console.log(process.env.PG_PASSWORD)
  }
})