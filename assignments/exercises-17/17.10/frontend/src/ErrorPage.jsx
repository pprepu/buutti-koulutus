import './ErrorPage.css'
import { useRouteError, Link } from 'react-router-dom'
const ErrorPage = props => {
  const error = useRouteError()

  if (error.status === 404) return (
    <div className='error-page'>
      <h2>404 - Not Found</h2>
      <Link to={'/'}>back to songs</Link>
    </div>
  )

  return (
    <div className='ErrorPage'>
      <h2>something terrible happened</h2>
    </div>
  )
}

export default ErrorPage