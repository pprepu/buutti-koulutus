import { useLoaderData } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { urlForSongs } from './App'

import './Song.css'

export const loader = async ({ params }) => {
  const { id } = params

  // maybe there's a better way than this... seems like I'm fetching songs too many times, but I don't know the solution yet
  const response = await fetch('/songs')
  const songs = await response.json()

  const songFound = songs.find(song => song.id === Number(id))
  if (!songFound) {
    const error = new Error
    error.status = 404
    throw error
  }

  return Number(id)
}

const Song = props => {
  const id = useLoaderData()
  const [currentSong, setCurrentSong] = useState(null) 

  const fetchSong = async id => {
    const response = await fetch(`${urlForSongs}/${id}`)
    try {
      const songFetched = await response.json()
      setCurrentSong(songFetched)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    if (id) {
      fetchSong(id)
    }
  }, [currentSong])

  if (!currentSong) {
    return null
  }

  const { title, lyrics } = currentSong
  return (
    <div className='song-page'>
      <h2>{title}</h2>
      <div className='song-lyrics-container'>
      {
        lyrics
          .split(/\n\n/)
          .filter(verse => verse.trim() !== '')
          .map((verse, i) => 
            <div key={i + verse.substring(0, 20)} className='song-lyrics-div'>{verse} </div>
          )
      }
      </div>
    </div>
  )
}

export default Song