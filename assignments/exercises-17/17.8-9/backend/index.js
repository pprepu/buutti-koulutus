import express from 'express'
import initialSongs from './songs.js'
import cors from 'cors'
import url from 'url'
import path from 'path'

import { validateFields } from './middlewares.js'

const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url))
const distDirectory = path.resolve(currentDirectory, './dist/')

let songs = [...initialSongs]

const generateIdForArray = array => Math.max(...array.map(item => item.id)) + 1

const PORT = process.env.PORT || 3000
const server = express()
server.use(cors())
server.use(express.json())
server.use(express.static(distDirectory))

server.get('/songs', (req, res) => {
  res.send(songs)
})

server.get('/songs/:id', (req, res) => {
  const id = Number(req.params.id)

  const songFound = songs.find(song => song.id === id)

  if (!songFound) {
    return res.status(404).send('could not find song with given id')
  }
  res.send(songFound)
})

server.post('/songs', validateFields, (req, res) => {
  const { title, lyrics } = req.body
  const newSong = {
    id: generateIdForArray(songs),
    title,
    lyrics
  }

  songs = songs.concat(newSong)

  res.status(201).send(newSong)
})

server.delete('/songs/:id', (req, res) => {
  const id = Number(req.params.id)

  const songsFiltered = songs.filter(song => song.id !== id)

  if (songsFiltered.length === songs.length) {
    return res.status(404).send('could not find song with given id')
  }
  songs = songsFiltered
  res.status(204).send()
})

server.put('/songs/:id', validateFields, (req, res) => {
  const id = Number(req.params.id)

  const currentSong = songs.find(song => song.id === id)

  if (!currentSong) {
    return res.status(404).send('could not find song with given id')
  }

  const { title, lyrics } = req.body

  const updatedSong = {
    id,
    title,
    lyrics
  }
  
  songs = songs.map(song => song.id === id ? updatedSong : song)

  res.status(201).send(updatedSong)
})

server.get('*', (req, res) => {
  res.sendFile('index.html', { root: distDirectory })
})


server.listen(PORT, () => {
  console.log('listening to port', PORT)
})