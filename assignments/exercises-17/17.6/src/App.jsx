import "./App.css"
import List from "./List"

const namesList = ["jack", "john", "anna", "rudolf"]

const decorations = ['bold', 'italic', 'both', 'none']

function App() {
  return (
    <div className="App">
      <div className="lists">
        {decorations.map(decor => <List key={decor} name={decor} elements={namesList} decoration={decor} />)}
      </div>
      {/* <List name={1} elements={namesList.map(name => Number(name))} decoration='hmm' /> */}
    </div>
  )
}

export default App