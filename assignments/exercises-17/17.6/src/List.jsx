import "./List.css"
import PropTypes from "prop-types"

const List = ({ name, elements, decoration }) => {
  let listStyle = {}

  if (decoration === 'bold' || decoration === 'both') {
    listStyle = {...listStyle, fontWeight: 'bold'}
  }

  if (decoration === 'italic' || decoration === 'both') {
    listStyle = {...listStyle, fontStyle: 'italic'}
  }

  return (
    <ul>
      <h2>{name}</h2>
      {elements.map((item, i) => (
        <li key={item + i.toString()} style={listStyle}>{item}</li>
      ))}
    </ul>
  )
}

List.propTypes = {
  name: PropTypes.string.isRequired,
  elements: PropTypes.arrayOf(PropTypes.string).isRequired,
  decoration: PropTypes.oneOfType([
    PropTypes.oneOf(["bold", "italic", "both", "none"]),
    PropTypes.instanceOf(null)
  ])
}

export default List
