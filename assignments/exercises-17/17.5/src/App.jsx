import { useState } from 'react'

function App() {

  const [hexString, setHexString] = useState('#ffffff')

  const toHex = primaryColor => {
    const colorInHex = primaryColor.toString(16)

    if (colorInHex.length < 2) {
      return "0" + colorInHex
    }

    return colorInHex
  }

  const rgbToHexString = (r, g, b) => '#' + toHex(r) + toHex(g) + toHex(b)

  const getRandomNumberBetweenZeroAndMax = max => Math.floor(Math.random() * (max + 1))

  const changeColor = () => {
    setHexString(rgbToHexString(
      getRandomNumberBetweenZeroAndMax(255), 
      getRandomNumberBetweenZeroAndMax(255), 
      getRandomNumberBetweenZeroAndMax(255)
    ))
  }

  const styleForBox = {
    border: 'solid 2px black',
    width: '40%',
    height: '40%',
    backgroundColor: hexString,
    borderRadius: '5px',

    transition: 'background-color 300ms ease-in'
  }

  return (
    <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100vw', height:'100vh', flexDirection: 'column'}}>
      {hexString}
      <div style={styleForBox} onClick={changeColor}></div>
    </div>
  )
}

export default App
