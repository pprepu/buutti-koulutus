import './App.css'

import { Outlet } from 'react-router-dom'
import SongList from './SongList'


function App() {
  return (
    <div className='app-page-container'>
      <h1>Song book</h1>
      <div className="app-page">
      
      <div className='app-left-column'>
        <SongList />
      </div>
      <div className='app-right-column'>
        <Outlet />
      </div>
    </div>
    </div>
    
  )
}

export default App