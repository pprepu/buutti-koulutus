import { useLoaderData } from 'react-router-dom'
import songs from './songs'

import './Song.css'

export const loader = ({ params }) => {
  const { id } = params

  const currentSong = songs.find(song => song.id === Number(id))
  if (!currentSong) {
    const error = new Error
    error.status = 404
    throw error
  }

  return currentSong
}

const Song = props => {
  const { id, title, lyrics } = useLoaderData()
  return (
    <div className='song-page'>
      <h2>{title}</h2>
      <div className='song-lyrics-container'>
      {
        lyrics
          .split(/\d\./)
          .filter(verse => verse.trim() !== '')
          .map((verse, i) => 
            <div key={i + verse.substring(0, 20)} className='song-lyrics-div'>{`${i + 1}. ${verse}`} </div>
          )
      }
      </div>
    </div>
  )
}

export default Song