import './ErrorPage.css'
import { useRouteError } from 'react-router-dom'

const ErrorPage = props => {

  const error = useRouteError()
  if (error.status === 404) {
    return (
      <div className='error-page'><h1>404 not found :/</h1></div>
    )
  }
  return (
    <div className='error-page'>
      something bad happened!
    </div>
  )
}

export default ErrorPage