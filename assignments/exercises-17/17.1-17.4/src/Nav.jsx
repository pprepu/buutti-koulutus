import './Nav.css'
import { Link, Outlet } from 'react-router-dom'

const Nav = ({ contactsInApp }) => {
  return (
    <div className='nav-page'>
      <nav>
        {contactsInApp.map(contact => <Link className='nav-link' key={contact.id} to={'/contact/' + contact.id}>{contact.name}</Link>)}
      </nav>

      <Outlet />
    </div>
  )
}

export default Nav