import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

import Contact, { loader as contactLoader } from './Contact'
import AdminPage from './AdminPage'
import Nav from './Nav'
import ErrorPage from './ErrorPage'

import contactsInApp from './contacts'

const router = createBrowserRouter([
  {
    path: '/',
    errorElement: <ErrorPage />,
    children: [
      {
        path: 'contact/',
        element: <Nav contactsInApp={contactsInApp} />,
        children: [
          {
            path: ':id',
            element: <Contact contactsInApp={contactsInApp} />,
            loader: contactLoader
          },

        ]
      },
      {
        path: 'admin',
        element: <AdminPage />
      }
    ]
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
