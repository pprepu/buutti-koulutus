import './Contact.css'
import { useLoaderData } from 'react-router-dom'
import contacts from './contacts'

export const loader = ({ params }) => {
  if (!contacts.find(contact => contact.id === Number(params.id))) {
    const error = new Error
    error.status = 404
    throw error
  }
  return Number(params.id)
} 

const Contact = ({ contactsInApp }) => {

  const id = useLoaderData()

  const currentContact = contactsInApp.find(contact => contact.id === id)
  // if (!currentContact) {
  //   return (
  //     <div className='contacts-page'>
  //     <h1>
  //     Contact page
  //     </h1>
  //     <h2>No contact found :(</h2>
  //   </div>
  //   )
  // }

  return (
    <div className='contacts-page'>
      <h1>
        Contact page
      </h1>
      <h2>{currentContact.name}</h2>
      {
      Object.entries(currentContact)
        .map(([fieldName, fieldValue]) => 
          fieldName === 'id' || fieldName === 'name'
            ? null 
            : <p key={fieldName+fieldValue}>{fieldName}: {fieldValue} </p>
          )
      }
    </div>
  )
}

export default Contact