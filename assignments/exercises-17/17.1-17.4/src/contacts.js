const contactsInApp = [
  {
    id: 1,
    name: 'Jarno',
    phone: '555445566',
    email: 'jarno@kinkku.fi'
  },
  {
    id: 2,
    name: 'Hanna',
    phone: '09445486876',
    email: 'hannanmaili@yahoo.com'
  },
  {
    id: 3,
    name: 'Roope',
    phone: '9764535435',
    email: 'mestaaja@hotmail.com'
  }
]

export default contactsInApp