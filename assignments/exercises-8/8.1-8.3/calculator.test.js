import { calculator } from "./calculator.js";

// 8.1
test("multiplying two positive values returns the correct positive value", () => {
  expect(calculator("*", 5, 6)).toBe(30);
});

test("multiplying two negative values returns the correct positive value", () => {
  expect(calculator("*", -10, -10)).toBe(100);
});

test("multiplying a negative and a positive values returns the correct negative value", () => {
  expect(calculator("*", -4, 7)).toBe(-28);
});

test("multiplying with 0 returns 0", () => {
  const testCases = [0, -2e100, 55, 890123, Number.MAX_VALUE];

  testCases.forEach(number => {
    expect(calculator("*", 0, number) === 0).toEqual(true);
    expect(calculator("*", number, 0) === 0).toEqual(true);
  })
})

test("if one of the operands is a string, function returns NaN", () => {
  expect(calculator("*", "4", 7)).toBe(NaN);
  expect(calculator("*", 4, "7")).toBe(NaN);
});

test("if one of the operands is undefined, function returns NaN", () => {
  expect(calculator("*", 3)).toBe(NaN);
  expect(calculator("*", undefined, 3)).toBe(NaN);
});

test("if one of the operands is null, function returns NaN", () => {
  expect(calculator("*", 3, null)).toBe(NaN);
  expect(calculator("*", null, 3)).toBe(NaN);

});

test("multiplying numbers with decimals returns correct values", () => {
  const delta = 1e-5;
  expect(calculator("*", 3.5, 2) - 7 < delta).toEqual(true);
  expect(calculator("*", 3.2222222, 2.1) - 6.76666666 < delta).toEqual(true);
  expect(calculator("*", 3, 1.5) - 4.5 < delta).toEqual(true);
});

test("when multiplying to a very, very high number, function returns Infinity", () => {
  expect(calculator("*", Number.MAX_VALUE, 2)).toBe(Infinity);
});

// 8.2
describe("division", () => {
  it("returns the correct number with two positive values", () => {
    expect(calculator("/", 4, 2)).toBe(2);
    expect(calculator("/", 10, 5)).toBe(2);
    expect(calculator("/", 100, 10)).toBe(10);
  }),
  it("returns the correct number with 1 negative value", () => {
    expect(calculator("/", -4, 2)).toBe(-2);
    expect(calculator("/", 10, -5)).toBe(-2);
    expect(calculator("/", 100, -10)).toBe(-10);
  }),
  it("returns the correct number with 2 negative values", () => {
    expect(calculator("/", -4, -2)).toBe(2);
    expect(calculator("/", -10, -5)).toBe(2);
    expect(calculator("/", -100, -10)).toBe(10);
  }),
  it("returns 0 if dividing zero with different numbers", () => {
    const testNumbers = [2.22, -1000000, 2e100, -0.5];
    testNumbers.forEach(number => {
      expect(calculator("/", 0, number) === 0).toEqual(true);
    });
  }),
  it("returns NaN if one of the operands is a string", () => {
    expect(calculator("/", -4, "-2")).toBe(NaN);
    expect(calculator("/", "-4", "-2")).toBe(NaN);
    expect(calculator("/", "-4", 2)).toBe(NaN);
  }),
  it("returns NaN if one of the operands is undefined", () => {
    expect(calculator("/", -4)).toBe(NaN);
    expect(calculator("/", undefined, -4)).toBe(NaN);
    expect(calculator("/")).toBe(NaN);
  }),
  it("returns NaN if one of the operands is null", () => {
    expect(calculator("/", null, 4)).toBe(NaN);
    expect(calculator("/", 4, null)).toBe(NaN);
    expect(calculator("/", null, null)).toBe(NaN);

  }),
  it("throws an error if dividing with 0", () => {
    expect(() => calculator("/", -4, 0)).toThrow();
    expect(() => calculator("/", 0, 0)).toThrow();
    expect(() => calculator("/", 125461, 0)).toThrow();
  })
});

// 8.3
test("addition works", () => {
  expect(calculator("+", 1, 2)).toBe(3);
});

test("subtraction works", () => {
  expect(calculator("-", 1, 2)).toBe(-1);
});

test("calculator returns NaN with unknown operator", () => {
  expect(calculator("**", 1, 2)).toBe(NaN);
});