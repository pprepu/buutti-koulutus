import { convert } from "./converter.js";

describe("convert", () => {
  it("returns correct values on typical valid cases", () => {
    const delta = 1e-2;
    expect(convert(4, "dl", "l")).toBe(0.4);
    expect(convert(0.4, "l", "dl")).toBe(4);

    expect(convert(20, "oz", "pint") - 1 < delta).toEqual(true);
    expect(convert(1, "pint", "oz") - 20 < delta).toEqual(true);

    expect(convert(2, "cup", "dl") - 5.68 < delta).toEqual(true);
    expect(convert(5.68, "dl", "cup") - 2 < delta).toEqual(true);

    expect(convert(5.55, "l", "oz") - 195.33 < delta).toEqual(true);
    expect(convert(195.33, "oz", "l") - 5.55 < delta).toEqual(true);


  });
  it("doesn't change the amount too much when source and target units are the same", () => {
    const delta = 1e-5;
    const units = ["l", "dl", "oz", "cup", "pint"];
    units.forEach(unit => {
      const randomNumber = Math.random() * 100;
      expect(convert(randomNumber, unit, unit) - randomNumber < delta).toEqual(true);
    });
  });
  it("returns NaN when source unit is not 'l', 'dl', 'oz', 'cup' or 'pint'", () => {
    expect(convert(14, "ml", "oz")).toEqual(NaN);
  });
  it("returns NaN when target unit is not 'l', 'dl', 'oz', 'cup' or 'pint'", () => {
    expect(convert(14, "dl", "ruokalusikka")).toEqual(NaN);
  });
  it("returns NaN when amount cannot be parsed to a number", () => {
    expect(convert("4 litraa", "l", "pint")).toEqual(NaN);
  });
});