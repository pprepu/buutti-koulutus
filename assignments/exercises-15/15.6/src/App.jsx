import { useState } from 'react'
import './App.css'

const H1Element = ({ string }) => <h1>Your string is: {string} </h1>
const InputElement = ({ value, setValue }) => <input value={value} onChange={event => setValue(event.target.value)} />

function App() {

  const [inputString, setInputString] = useState('')
  const [stringForH1, setStringForH1] = useState('')

  return (
    <div className="App">
      <H1Element string={stringForH1} />
      <InputElement value={inputString} setValue={setInputString} />
      <button onClick={() => setStringForH1(inputString)}>Submit</button>
    </div>
  )
}

export default App