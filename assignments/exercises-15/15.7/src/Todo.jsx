import './Todo.css'

const Todo = ({ item, isDone, toggleTodo, handleClick }) => {
  return (
    <>
      <li className={isDone ? 'done' : 'notDone'} onClick={toggleTodo}>{item}</li>
      <button className='todoButton' onClick={handleClick}>remove</button>
    </>
  )
}

export default Todo