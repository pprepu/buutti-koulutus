import { useState } from 'react'
import './App.css'

import Todo from './Todo'

function App() {
  const [todos, setTodos] = useState([])
  const [input, setInput] = useState('')

  const addTodo = text => {
    if (!text) {
      return
    }
    setTodos([...todos, { item: text, isDone: false}])
    setInput('')
  }

  const removeTodo = index => {
    const filteredTodos = todos.filter((_, i) => i !== index)
    setTodos(filteredTodos)
  }

  const toggleTodo = index => {
    const modifiedTodos = todos.map((todo, i) => i !== index ? todo : {...todo, isDone: !todo.isDone })
    setTodos(modifiedTodos)
  }

  return (
    <div className='App'>
      <h1>todos</h1>
      <p className='instructions'>click todos to mark them as done / not done</p>
      <div className='myForm'>
        <input value={input} onChange={event => setInput(event.target.value)} />
        <button onClick={() => addTodo(input)}>add</button>
      </div>
      {todos.length === 0 
        ? <p>No todos!</p>
        : <ul> 
            {todos.map((todo, index) => 
              <Todo 
                key={index+todo.item} 
                item={todo.item} 
                toggleTodo={() => toggleTodo(index)}  
                handleClick={() => removeTodo(index)}
                isDone={todo.isDone}
              />
            )} 
          </ul>
      }
    </div>
  )
}

export default App
