import { useState } from 'react'
import './App.css'

function App() {

  const [value1, setValue1] = useState(0)
  const [value2, setValue2] = useState(0)
  const [value3, setValue3] = useState(0)

  return (
    <div className="App">
      <button onClick={() => setValue1(value1 + 1)}>{value1}</button>
      <button onClick={() => setValue2(value2 + 1)}>{value2}</button>
      <button onClick={() => setValue3(value3 + 1)}>{value3}</button>
      <p>
        {value1 + value2 + value3}
      </p>
    </div>
  )
}

export default App