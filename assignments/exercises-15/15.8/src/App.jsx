import { useState } from 'react'
import './App.css'
import Grid from './components/Grid'

const ROWS = 3

function App() {
  const [grid, setGrid] = useState(Array(ROWS * ROWS).fill(''))
  const [gameIsOver, setGameIsOver] = useState(false)
  const [playerTurn, setPlayerTurn] = useState("X")
  const [scoreX, setScoreX] = useState(0)
  const [scoreO, setscoreO] = useState(0)
  const [playerWon, setPlayerWon] = useState(null)


  const handlePlayerWinning = player => {
    if (player === 'X') {
      setScoreX(scoreX + 1)
    }

    if (player === 'O') {
      setscoreO(scoreO + 1)
    }

    setPlayerWon(player)
  }

  const changePlayerTurn = () => {
    setPlayerTurn(playerTurn === 'X' ? 'O' : 'X')
  }

  const resetGame = () => {
    setGrid(Array(ROWS * ROWS).fill(''))
    setGameIsOver(false)
    setPlayerWon(null)
  }

  const gameOverTextElement = () => playerWon ? <h2> Game over, {playerWon} won</h2> : <h2>Game over, draw</h2>

  return (
    <div className="app">
      <h1>tic tac toe</h1>
      <p>score - X: {scoreX}, O: {scoreO}</p>
      <p>turn: <b>{playerTurn}</b></p>
      <div className="gridContainer">
        <Grid 
          rows={ROWS} 
          grid={grid} 
          setGrid={setGrid} 
          gameIsOver={gameIsOver} 
          setGameIsOver={setGameIsOver} 
          playerTurn={playerTurn} 
          changePlayerTurn={changePlayerTurn}
          handlePlayerWinning={handlePlayerWinning}
        />
        <button onClick={() => resetGame()}>reset</button>
      </div>
      <div className="gameOverText">
        {gameIsOver && gameOverTextElement() }
      </div>
    </div>
  )
}

export default App