import { useState } from 'react'
import './Cell.css'

const Cell = ({ content, handleClick, isInWinningCombination, gameIsOver }) => {
  
  const [hover, setHover] = useState(false)

  if (isInWinningCombination) {
    return (
      <div className="cell winning" onClick={handleClick}>
      {content}
    </div>
    )
  }

  const hoverStyles = {
    border: 'solid 3px rgb(67, 64, 64)'
  }

  return (
    <div 
      className="cell" 
      onClick={handleClick} 
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      style={(hover && !gameIsOver) ? hoverStyles : {}} >
      {content}
    </div>
  )
}

export default Cell