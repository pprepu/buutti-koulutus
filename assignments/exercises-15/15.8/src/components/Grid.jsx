import { useState } from 'react'
import Cell from './Cell'
import './Grid.css'

let id = 1;

const Grid = ({ rows, grid, setGrid, gameIsOver, setGameIsOver, changePlayerTurn, playerTurn, handlePlayerWinning }) => {
  // used for adding styles for cells that form the winning combination
  const [winningCells, setWinningCells] = useState([])

  // using exactly the same logic for checking win conditions I used in assignment 6.3 (except for diagonals, where I fixed a bug)
  // is dynamic in nature, so this app could quite easily be expanded into generating different sized grids
  // https://gitlab.com/pprepu/buutti-koulutus/-/blob/main/assignments/exercises-6/6.3/script.js for detailed comments
  const checkRowsForWin = updatedGrid => {
    for (let startingIndex = 0; startingIndex < updatedGrid.length; startingIndex += rows) {
      let currentRow = ''
      const indexesForCells = []

      for (let rowIndex = startingIndex; rowIndex < rows + startingIndex; rowIndex++) {
        currentRow += updatedGrid[rowIndex]
        indexesForCells.push(rowIndex)
      }

      if (currentRow.length === rows && currentRow.split('').every(char => char === currentRow[0])) {
        setWinningCells(indexesForCells)
        return true
      }
    }
    return false
  }

  const checkColumnsForWin = updatedGrid => {
    for (let startingIndex = 0; startingIndex < rows; startingIndex ++) {
      let currentColumn = ''
      const indexesForCells = []
      for (let columnIndex = startingIndex; columnIndex < updatedGrid.length; columnIndex += rows) {
        currentColumn += updatedGrid[columnIndex]
        indexesForCells.push(columnIndex)
      }

      if (currentColumn.length === rows && currentColumn.split('').every(char => char === currentColumn[0])) {
        setWinningCells(indexesForCells)
        return true
      }
    }
    return false
  }

  const checkDiagonalsForWin = updatedGrid => {
    // 1st diagonal
    let currentDiagonal = ''
    let indexesForCells = []
    for (let index = 0; index < updatedGrid.length; index += rows + 1) {
      currentDiagonal += updatedGrid[index]
      indexesForCells.push(index)
    }
    if (currentDiagonal.length === rows && currentDiagonal.split('').every(char => char === currentDiagonal[0])) {
      setWinningCells(indexesForCells)
      return true
    }
    
    // 2nd diagonal
    currentDiagonal = ''
    indexesForCells = []
    // updatedGrid.length - rows + 1 is the first column on last row which is the last cell for this check
    for (let index = rows - 1; index < updatedGrid.length - rows + 1; index += rows - 1) {
      currentDiagonal += updatedGrid[index]
      indexesForCells.push(index)
    }
    if (currentDiagonal.length === rows && currentDiagonal.split('').every(char => char === currentDiagonal[0])) {
      setWinningCells(indexesForCells)
      return true
    }
  
    return false
  }

  const gridIsFull = updatedGrid => updatedGrid.join('').length === rows * rows

  const playerHasWon = updatedGrid => 
    checkRowsForWin(updatedGrid) || 
    checkColumnsForWin(updatedGrid) || 
    checkDiagonalsForWin(updatedGrid)

  const handleClick = index => {
    if (gameIsOver || grid[index]) {
      return
    }
    
    const updatedGrid = [...grid]
    updatedGrid[index] = playerTurn

    // no room for more moves and final move didn't cause a win
    if (gridIsFull(updatedGrid) && !playerHasWon(updatedGrid)) {
      setWinningCells([])
      setGameIsOver(true)
    }

    if (playerHasWon(updatedGrid)) {
      setGameIsOver(true)
      handlePlayerWinning(playerTurn)
    }
    
    setGrid(updatedGrid)
    changePlayerTurn()
  }

  return (
    <div className="grid">
      {grid.map((content, index) => 
        <Cell
          key={id++}
          content={content} 
          handleClick={() => handleClick(index)}
          gameIsOver={gameIsOver}
          isInWinningCombination={gameIsOver && winningCells.includes(index)}
        />
      )}
    </div>
  )
}

export default Grid