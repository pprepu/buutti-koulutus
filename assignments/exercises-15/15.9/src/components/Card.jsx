import { useState } from 'react'
import './Card.css'

const Card = ({ image, found, revealed, handleClick }) => {

  // return (
  //   <div className={found ? 'card found' : 'card'} onClick={handleClick}>
  //       {revealed && 
  //         <img className={revealed ? 'revealed' : 'notReleaved'} src={image} /> 
  //       }
  //   </div>
  // )
  return (
    <div className={found ? 'card found' : 'card'} onClick={handleClick}>
      <img className={revealed ? 'revealed' : 'notRevealed'} src={image} />
    </div>
  )
}

export default Card