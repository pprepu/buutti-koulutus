import { useState } from 'react'
import { DECK_MAX_SIZE, DECK_MIN_SIZE } from '../services/deck'

const Reset = ({ resetGame, defaultSize }) => {
  const [deckSize, setDeckSize] = useState(defaultSize)

  const deckSizeDown = () => {
    if (deckSize > DECK_MIN_SIZE) {
      setDeckSize(deckSize - 1)
    }
  }

  const deckSizeUp = () => {
    if (deckSize < DECK_MAX_SIZE) {
      setDeckSize(deckSize + 1)
    }
  }
  return (
    <div className='reset'>
      <div className='resetRow'>
        number of images
        <button onClick={deckSizeDown}>-</button>
        {deckSize}
        <button onClick={deckSizeUp}>+</button>
      </div>
      <button onClick={() => resetGame(deckSize)}>reset</button>
    </div>
  )
}

export default Reset