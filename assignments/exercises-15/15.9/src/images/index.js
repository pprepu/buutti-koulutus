import img_1 from './img_1.svg'
import img_2 from './img_2.svg'
import img_3 from './img_3.svg'
import img_4 from './img_4.svg'
import img_5 from './img_5.svg'
import img_6 from './img_6.svg'
import img_7 from './img_7.svg'
import img_8 from './img_8.svg'
import img_9 from './img_9.svg'
import img_10 from './img_10.svg'
import img_11 from './img_11.svg'
import img_12 from './img_12.svg'
import img_13 from './img_13.svg'
import img_14 from './img_14.svg'
import img_15 from './img_15.svg'
import img_16 from './img_16.svg'
import img_17 from './img_17.svg'
import img_18 from './img_18.svg'
import img_19 from './img_19.svg'
import img_20 from './img_20.svg'

export default [
  img_1,
  img_2,
  img_3,
  img_4,
  img_5,
  img_6,
  img_7,
  img_8,
  img_9,
  img_10,
  img_11,
  img_12,
  img_13,
  img_14,
  img_15,
  img_16,
  img_17,
  img_18,
  img_19,
  img_20,
]