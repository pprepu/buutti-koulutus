import { useState } from 'react'
import './App.css'
import Card from './components/Card'
import Reset from './components/Reset'
import { createDeck } from './services/deck'

const DEFAULT_DECK_SIZE = 6

const App = () => {
  const [deck, setDeck] = useState(createDeck(DEFAULT_DECK_SIZE))
  const [firstCardRevealed, setFirstCardRevealed] = useState(null)
  const [turnsPlayed, setTurnsPlayed] = useState(0)
  const [turnIsEnding, setTurnIsEnding] = useState(false)
  const [gameIsOver, setGameIsOver] = useState(false)

  const hideRemainingCardsInDeck = deckReceived => deckReceived.map(card => card.found ? card : {...card, revealed: false})

  const endTurn = (currentDeck) => { 
    setDeck(hideRemainingCardsInDeck(currentDeck))
    setTurnIsEnding(false)
  }

  const isGameOver = currentDeck => currentDeck.filter(card => card.found).length === deck.length

  const handleTurn = (currentDeck, card1, card2) => {
    setTurnsPlayed(turnsPlayed + 1)
    setFirstCardRevealed(null)

    // id check is already done in revealCard function, but keeping it here just in case something gets changed
    const matchBetweenCards = card1.image === card2.image && card1.id !== card2.id
    if (matchBetweenCards) { 
      currentDeck = currentDeck.map(card => card.image === card1.image ? {...card, found: true} : card)
      if (isGameOver(currentDeck)) {
        setGameIsOver(true)
      }
      endTurn(currentDeck)
      return
    } 
    setDeck(currentDeck)
    setTimeout(() => endTurn(currentDeck), 1000)
  }

  const revealCard = cardId => {
    // using currentDeck variable and passing it around functions to avoid some bugs I ran into with only using deck/setDeck
    let currentDeck = [...deck]
    const currentCard = currentDeck.find(card => card.id === cardId)

    // clicking already found cards shouldn't do anything
    if (currentCard.found) {
      return
    }

    // 1st card of the turn, no comparisons need to be made
    if (!firstCardRevealed) {
      setDeck(currentDeck.map(card => card.id === cardId ? {...card, revealed: true} : card))
      setFirstCardRevealed(currentCard)
      return
    }
    // 2nd card but player has decided to click an already revealed card
    if (currentCard.id === firstCardRevealed.id) {
      return
    }
    
    currentDeck = currentDeck.map(card => card.id === cardId || card.id === firstCardRevealed.id ? { ...card, revealed: true } : card)
    // when turnIsEnding, cards cannot be clicked (turn has to be resolved before it is allowed again)
    setTurnIsEnding(true)
    handleTurn(currentDeck, firstCardRevealed, currentCard)
  }

  const debug = () => {
    console.log('debug deck', deck)
  }

  const resetGame = (newDeckSize = DEFAULT_DECK_SIZE) => {
    // currently impossible to reset game while between turns to prevent some bugs
    // I _should_ refactor resetGame to use clearTimeout instead and allow resetting whenever 
    if (turnIsEnding) {
      return
    }
    setDeck(createDeck(newDeckSize))
    setGameIsOver(false)
    setTurnsPlayed(0)
    setFirstCardRevealed(null)
    setTurnIsEnding(false)
  }

  // a very rough version
  const howManyColumnsForGrid = cardsInDeck => {
    if (cardsInDeck < 5) {
      return 2
    }

    if (cardsInDeck < 9) {
      return 3
    }

    if (cardsInDeck < 17) {
      return 4
    }

    if (cardsInDeck < 21) {
      return 5
    }

    if (cardsInDeck < 26) {
      return 6
    }

    if (cardsInDeck < 35) {
      return 7
    }

    return 8
  }

  const createStyleForGridWithDynamicNumberOfColumns = cardsInDeck => {
    const style = {
      gridTemplateColumns: ''
    }
    for (let i = 0; i < howManyColumnsForGrid(cardsInDeck); i++) {
      style.gridTemplateColumns = style.gridTemplateColumns + 'auto '
    }

    return style
  }
  
  return (
    <div className='App'>
      <div>
        <p>turns played: {turnsPlayed} </p>
      </div>
      <div className='grid' style={createStyleForGridWithDynamicNumberOfColumns(deck.length)}>
        {deck.map((card) => 
          <Card 
            key={card.id} 
            image={card.image} 
            found={card.found}
            revealed={card.revealed}
            handleClick={!turnIsEnding ? () => revealCard(card.id) : null}
          />
        )}
      </div>
      <Reset resetGame={resetGame} defaultSize={DEFAULT_DECK_SIZE} />
      {gameIsOver && <p className='gameOverText'>Well played!</p>}
      <button className="debug" onClick={debug}>debug</button>
    </div>
  )
}

export default App
