import images from '../images'
export const DECK_MIN_SIZE = 2
// max size increases every time I'm motivated enough to fetch some more images!
export const DECK_MAX_SIZE = images.length

// using fisher-yates shuffle algorithm
const createNewShuffledArray = array => {
  const arrayShuffled = [...array]

  let currentIndex = arrayShuffled.length
  let randomIndex = null

  while (currentIndex !== 0) {

    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex--
    // destructuring swaps
    [ arrayShuffled[currentIndex], arrayShuffled[randomIndex] ] = [ arrayShuffled[randomIndex], arrayShuffled[currentIndex] ]
  }

  return arrayShuffled
}

export const createDeck = numberOfImages => {
  if (numberOfImages < DECK_MIN_SIZE) {
    console.log('services/deck.js - app tried to go under min deck size')
    numberOfImages = DECK_MIN_SIZE
  }
  if (numberOfImages > DECK_MAX_SIZE) {
    console.log('services/deck.js - app tried to go over max deck size')
    numberOfImages = DECK_MAX_SIZE
  }
  // shuffle images first so they are not the same every time
  const shuffledImages = createNewShuffledArray(images)
  const newDeck = []
  let timesAllImagesAdded = 0
  let id = 1
  // add every image twice because we need them as pairs for the game
  while (timesAllImagesAdded < 2) {
    for (let i = 0; i < numberOfImages; i++) {
      newDeck.push({
        image: shuffledImages[i],
        revealed: false,
        found: false,
        id: id++
      })
    }

    timesAllImagesAdded++
  }
  
  return createNewShuffledArray(newDeck)
}