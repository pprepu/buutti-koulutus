const Name = ({ name, isEven }) => {
  const textStyle = {
    fontStyle: isEven ? 'normal' : 'italic',
    fontWeight: isEven ? 'bold' : 'normal'
  }
  return (
    <li style={textStyle}>
      {name}
    </li>
  )
}

const NameList = ({ names }) => {
  return (
    <>
      <h2>Names:</h2>
      <ul>
        {
          names
            .map((name, index) => 
              <Name key={index + name} name={name} isEven={index % 2 === 0} />
            )
          }
      </ul>
    </>
  )
}

export default NameList