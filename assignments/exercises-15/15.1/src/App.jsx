import Header from './components/Header'
import Year from './components/Year'
import NameList from './components/NameList'

function App() {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]

  return (
    <div className="App">
      <Header content='Hello React!' />
      <Year />
      <NameList names={namelist}/>
    </div>
  )
}

export default App