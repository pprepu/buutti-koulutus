const days = 365 // in a year
const hours = 24  // in a day
const seconds = 60 * 60 // in an hour

const secondsInAYear = days * hours * seconds

console.log('total amount of seconds in a year:', secondsInAYear)