const modifyString = string => {

  string = string.trim()
  
  return string.substring(0, 1).toLowerCase() + string.substring(1, 20)
}

const checkString = string => {
  const errorMessage = 'invalid formatting - your argument has NOT passed given conditions'

  if (
    string !== string.trim() ||
    string.length > 20 ||
    string.substring(0, 1).toLowerCase() !== string.substring(0, 1)
    ) {
    return errorMessage
  }

  return `Your string:\n${string}\n..passed all tests`

}

const args = process.argv.slice(2)

if (args.length === 1) {
  console.log(modifyString(args[0]))
} else if (args.length > 1 && args[1] === 'check') {
  console.log(checkString(args[0]))
} else {
  console.log('___how to use this program___')
  console.log('if you just want to modify a string: provide the string as the only argument')
  console.log('if you want to check a string against the conditions, provide a 2nd argument "check"')
  console.log('example: node exercise1.5 "this is my string" "check"')
}
