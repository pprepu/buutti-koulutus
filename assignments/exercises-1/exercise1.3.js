const str1 = 'merkkijono'
const str2 = 'teksti'

const str_sum = str1 + str2
console.log(str_sum)
console.log(str1.length)
console.log(str2.length)
const str_avg = (str1.length + str2.length) / 2
console.log('avg', str_avg)

if (str1.length < str_avg) {
  console.log(str1)
}

if (str2.length < str_avg) {
  console.log(str2)
}

if (str_sum.length < str_avg) {
  console.log(str_sum)
}