const basicMath = (a, b) => {
  console.log(`--- a: ${a}, b: ${b} ---`)
  console.log('sum:', a + b)
  console.log('difference:', a - b)
  console.log('fraction:', a / b)
  console.log('product:', a * b)
  console.log('exponention:', a ** b)
  console.log('modulo:', a % b)
  console.log('average:', (a + b) / 2)
}

basicMath(2, 5)
basicMath(14, 0)

// command linen lukeminen
const args = process.argv.slice(2)

if (args.length === 2) {
  basicMath(Number(args[0]), Number(args[1]))
} else {
  console.log('command line reading failed - wrong amount of arguments')
}