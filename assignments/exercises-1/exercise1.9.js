if (process.argv.length < 3) {
  console.log('you need to provide an argument for the length of a side')
} else {
  const side = process.argv[2]
  !isNaN(side)
    ? console.log(`square area size is ${side * side}m2 with a side length of ${side}m.`)
    : console.log('error: the argument provided was NOT a number')
}