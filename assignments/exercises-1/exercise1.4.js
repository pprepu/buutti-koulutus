const playerCount = 7

if (playerCount === 4) {
  console.log('hearts can be played')
}

const isStressed = true
const hasIceCream = true

if (!isStressed || hasIceCream) {
  console.log('mark is happy')
}

const sunIsShining = true
const isRaining = false
const temperature = 20

if (sunIsShining && !isRaining && temperature >= 20) {
  console.log('beach day')
}

const isTuesday = true
const seesSuzy = true
const seesDan = false

if (isTuesday && ((seesSuzy && !seesDan) || (!seesSuzy && seesDan))) {
  console.log('arin is happy')
}