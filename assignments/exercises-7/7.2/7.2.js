console.log(3);

new Promise((resolve, _reject) => {
  setTimeout(() => {
    console.log("..2");
    resolve();
  }, 1000);
}).then(() => new Promise((resolve, _reject) => {
  setTimeout(() => {
    console.log("....1");
    resolve();
  }, 1000);
})).then(() => {
  setTimeout(() => console.log("GO!"), 1000);
});