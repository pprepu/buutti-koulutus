// 3 2 1 GO!

// v1
// const arr = [3, 2, 1 ,"GO"];
// arr.forEach((value, i) => setTimeout(() => console.log(value), i*1000));

// v2
// console.log("3");
// setTimeout(() => {
//   console.log("2");
//   setTimeout(() => {
//     console.log("1");
//     setTimeout(() => {
//       console.log("GO");
//     }, 1000);
//   }, 1000);
// }, 1000);

// v3
const recursiveCounter = n => {
  if(n === 0) {
    console.log("GO!");
  } else {
    console.log(n);
    setTimeout(() => recursiveCounter(n - 1), 1000);
  }
};
// v4
const recursiveCounterWithNicerPrinting = (n, dots) => {
  if(n === 0) {
    console.log("GO!");
  } else {
    let dotsString = "";
    for (let i = 0; i < dots; i++) {
      dotsString += ".";
    }
    console.log(dotsString + n);
    setTimeout(() => recursiveCounterWithNicerPrinting(n - 1, dots + 2), 1000);
  }
};

recursiveCounterWithNicerPrinting(5, 0);