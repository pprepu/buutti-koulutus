import axios from "axios";
import fileService from "../../services/files.js";

const baseUrl = "https://jsonplaceholder.typicode.com";

const getTodos = async () => {
  try {
    const response = await axios.get(`${baseUrl}/todos`);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

const getUsers = async () => {
  try {
    const response = await axios.get(`${baseUrl}/users`);
    return response.data;
  } catch (error) {
    console.log(error);
  }
};

// create an object, where keys are user IDs and values are an object with the user info we want
const createMapFromUserArray = userArray => userArray
  .reduce((userMap, user) => {
    userMap[user.id] = { 
      name: user.name, 
      username: user.username, 
      email: user.email 
    };
    return userMap;
  }, {});

const combineUsersWithTodos = async () => {
  const todos = await getTodos();
  const users = await getUsers();
  if (!(todos && users)) {
    console.error("could not fetch some of the data!");
    return;
  }

  const userMap = createMapFromUserArray(users);

  const todosMappedWithUsers = todos
    .map(todo => {
      return {
        user: userMap[todo.userId],
        id: todo.id,
        title: todo.title,
        completed: todo.completed
      };
    });

  return todosMappedWithUsers;
};

const fetchAndCombineDataThenPrintAndSaveIt = async () => {
  const combinedArray = await combineUsersWithTodos();
  console.log(combinedArray);
  if (combinedArray) {
    console.log("modified data can be found in 7.6.json");
    fileService.stringifyObjectAndSaveAsJson(combinedArray);
  }
};

fetchAndCombineDataThenPrintAndSaveIt();