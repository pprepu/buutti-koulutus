const convertInputsAndCollectThemToObject = event => {
  return {
    gender: event.target.gender.value,
    weight: Number(event.target.weight.value),
    time: Number(event.target.time.value),
    drinkSize: Number(event.target.drinkSize.value),
    alcoholVolume: Number(event.target.alcoholVolume.value),
    doses: Number(event.target.doses.value)
  };
};

const calculateBAC = inputObject => {
  const litres = inputObject.drinkSize * inputObject.doses / 1000;
  const grams = litres * 8 * inputObject.alcoholVolume;
  const burning = inputObject.weight / 10;
  const gramsLeft = grams - (burning * inputObject.time);
  // cannot burn alcohol when none exist in blood -> cannot go below 0
  if (gramsLeft < 0) {
    return 0;
  }

  return inputObject.gender === "male"
    ? gramsLeft / (inputObject.weight * 0.7)
    : gramsLeft / (inputObject.weight * 0.6);
};

const inputIsValid = inputObject => {
  for (const currentValue of Object.values(inputObject)) {
    if (
      currentValue === "" ||
      (typeof currentValue === "number" && isNaN(currentValue)) ||
      (typeof currentValue === "number" && currentValue < 0)
    ) {
      return false;
    }
  }
  // special case (other number values can be zero)
  if (inputObject.weight === 0) {
    return false;
  }

  return true;
};

const setOutputText = string => {
  const output = document.querySelector(".output");
  output.innerText = string;
};

const form = document.querySelector("form");
form.addEventListener("submit", event => {
  event.preventDefault();
  const inputObject = convertInputsAndCollectThemToObject(event);
  if (inputIsValid(inputObject)) {
    setOutputText("Your BAC is " + calculateBAC(inputObject).toFixed(2));
  } else {
    setOutputText("Please fill the whole form with valid values.");
  }
});

document.querySelector("#resetButton").onclick = (event) => {
  event.preventDefault();
  setOutputText("Not calculated");
  form.reset();
};