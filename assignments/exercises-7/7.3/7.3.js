const getValue = function () {
  return new Promise((resolve, _reject) => {
    setTimeout(() => {
      resolve({ value: Math.random() });
    }, Math.random() * 1500);
  });
};

// v1 - async&await
const printTwoRandomValues = async () => {
  const { value: value1 } = await getValue();
  const { value: value2 } = await getValue();
  console.log(`V1: Value 1 is ${value1} and value 2 is ${value2}`);
};
printTwoRandomValues();
// v2 - promise.then()
Promise.all([getValue(), getValue()])
  .then(arr => arr.map(object => object.value))
  .then(mappedArr => console.log(`V2: Value 1 is ${mappedArr[0]} and value 2 is ${mappedArr[1]}`));