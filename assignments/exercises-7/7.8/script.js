// helper functions for event listeners
const getFormula = () => document.querySelector(".formula").innerText;

const getLastCharOfFormula = () => {
  const currentFormula = getFormula();
  return currentFormula[currentFormula.length - 1];
};

const setFormula = string => {
  document.querySelector(".formula").innerText = string;
};

const replaceLastCharOfFormula = newChar => {
  const currentFormula = getFormula();
  setFormula(currentFormula.substring(0, currentFormula.length - 1) + newChar);
};

const hasOperator = string => new RegExp("[+*/-]").test(string);

const splitFormulaToPartsAndReturnObject = formula => {
  const operator = formula.split("").find(char => hasOperator(char));
  const operands = formula.split(operator);
  return { operator, operands };
};

const setErrorText = string => {
  const errorDiv = document.querySelector(".error");
  errorDiv.innerText = string;
  setTimeout(() => errorDiv.innerText = "", 2500);
};

const calculate = formulaObject => {
  const [operand1, operand2] = formulaObject.operands.map(operand => Number(operand));
  switch(formulaObject.operator) {
  case "+":
    return operand1 + operand2;
  case "-":
    return operand1 - operand2;
  case "*":
    return operand1 * operand2;
  case "/":
    return operand1 / operand2;
  default:
    return NaN;
  }
};

// eventListeners
const numberButtons = document.querySelectorAll(".number");
for (const numberButton of numberButtons) {
  numberButton.onclick = () => {
    const currentFormula = getFormula();
    if (currentFormula === "0") {
      setFormula(numberButton.innerText);
    } else {
      setFormula(currentFormula + numberButton.innerText);
    }
  };
}

const operatorButtons = document.querySelectorAll(".operator");
for (const operatorButton of operatorButtons) {
  operatorButton.onclick = () => {
    const currentFormula = getFormula();
    if (hasOperator(getLastCharOfFormula())) {
      replaceLastCharOfFormula(operatorButton.innerText);
    } else if (hasOperator(currentFormula)) { // for non-extra version
      return;
    } else {
      setFormula(currentFormula + operatorButton.innerText);
    }
  };
}

document.querySelector(".dot").onclick = () => {
  const currentFormula = getFormula();
  const lastCharOfFormula = getLastCharOfFormula();
  const operandArray = splitFormulaToPartsAndReturnObject(currentFormula).operands;
  const currentOperand = operandArray[operandArray.length - 1];
  if ("+-*/".includes(lastCharOfFormula) || currentOperand.includes(".")) {
    return;
  }
  setFormula(currentFormula + ".");
};

document.querySelector(".clear").onclick = () => {
  setFormula("0");
};

document.querySelector(".equals").onclick = () => {
  const formulaObject = splitFormulaToPartsAndReturnObject(getFormula());
  if (formulaObject.operands.length < 2 || formulaObject.operands[1] === "") {
    setErrorText("Please provide two operands with an operator");
    return;
  }
  const calcResult = calculate(formulaObject);
  if (isNaN(calcResult) || calcResult === Infinity) {
    setErrorText("Impossible to calculate");
    setFormula("0");
    return;
  }
  setFormula(calcResult);
};

// for debugging - currently not displayed
document.getElementById("debugButton").onclick = () => {
  const currentFormula = getFormula();
  console.log(splitFormulaToPartsAndReturnObject(currentFormula));
};