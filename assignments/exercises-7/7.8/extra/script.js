// helper functions for event listeners
const getFormula = () => document.querySelector(".formula").innerText;

const getLastCharOfFormula = () => {
  const currentFormula = getFormula();
  return currentFormula[currentFormula.length - 1];
};

const setFormula = string => {
  document.querySelector(".formula").innerText = string;
};

const replaceLastCharOfFormula = newChar => {
  const currentFormula = getFormula();
  setFormula(currentFormula.substring(0, currentFormula.length - 1) + newChar);
};

const hasOperator = string => new RegExp("[+*/-]").test(string);

const splitFormulaToPartsAndReturnObject = formula => {
  const operators = formula.split("").filter(char => hasOperator(char));
  const operands = formula.split(/[+*/-]/);
  return { operators, operands };
};

const setErrorText = string => {
  const errorDiv = document.querySelector(".error");
  errorDiv.innerText = string;
  setTimeout(() => errorDiv.innerText = "", 2500);
};

const calculateTwoValuesWithOperator = (value1, value2, operator) => {
  switch(operator) {
  case "+":
    return value1 + value2;
  case "-":
    return value1 - value2;
  case "*":
    return value1 * value2;
  case "/":
    return value1 / value2;
  default:
    return NaN;
  }
};

const splitFormulaByOperators = (formula, operators) => {
  return formula.split(new RegExp(`[${operators}]`));
};

const filterOperators = (formula, operators) => formula.split("").filter(char => operators.includes(char));

// I need to refactor this.. not very clear what's happening
const calculate = formula => {
  const addAndSubtractOperators = filterOperators(formula, "+-");
  let formulaPartlyCalculated = splitFormulaByOperators(formula, "+-")
    .map(splitFormula => {
      const multiAndDivideOperators = filterOperators(splitFormula, "*/");
      if (multiAndDivideOperators.length === 0) {
        return splitFormula;
      }
      const valuesToBeMultipliedOrDivided = splitFormulaByOperators(splitFormula, "*/").map(value => Number(value));
      return multiAndDivideOperators
        .reduce((acc, cur, index) => calculateTwoValuesWithOperator(acc, valuesToBeMultipliedOrDivided[index+1], cur), valuesToBeMultipliedOrDivided[0]);
    });
  formulaPartlyCalculated = formulaPartlyCalculated.map(value => Number(value));
  if (formulaPartlyCalculated.length > 1) {
    return addAndSubtractOperators
      .reduce((acc, cur, index) => calculateTwoValuesWithOperator(acc, formulaPartlyCalculated[index+1], cur), formulaPartlyCalculated[0])
  } else {
    return formulaPartlyCalculated[0];
  }
};

// eventListeners
const numberButtons = document.querySelectorAll(".number");
for (const numberButton of numberButtons) {
  numberButton.onclick = () => {
    const currentFormula = getFormula();
    if (currentFormula === "0") {
      setFormula(numberButton.innerText);
    } else {
      setFormula(currentFormula + numberButton.innerText);
    }
  };
}

const operatorButtons = document.querySelectorAll(".operator");
for (const operatorButton of operatorButtons) {
  operatorButton.onclick = () => {
    const currentFormula = getFormula();
    if (hasOperator(getLastCharOfFormula())) {
      replaceLastCharOfFormula(operatorButton.innerText);
    } else {
      setFormula(currentFormula + operatorButton.innerText);
    }
  };
}

document.querySelector(".dot").onclick = () => {
  const currentFormula = getFormula();
  const lastCharOfFormula = getLastCharOfFormula();
  const operandArray = splitFormulaToPartsAndReturnObject(currentFormula).operands;
  const currentOperand = operandArray[operandArray.length - 1];
  if ("+-*/".includes(lastCharOfFormula) || currentOperand.includes(".")) {
    return;
  }
  setFormula(currentFormula + ".");
};

document.querySelector(".clear").onclick = () => {
  setFormula("0");
};

document.querySelector(".equals").onclick = () => {
  const currentFormula = getFormula();
  const formulaObject = splitFormulaToPartsAndReturnObject(currentFormula);
  if (formulaObject.operands.length < 2 || formulaObject.operands[1] === "") {
    setErrorText("Please provide at last two operands with an operator");
    return;
  }

  if (!/\d/.test(getLastCharOfFormula())) {
    setErrorText("Invalid formula.");
    return;
  }
  const calcResult = calculate(currentFormula);
  if (isNaN(calcResult) || calcResult === Infinity) {
    setErrorText("Impossible to calculate");
    setFormula("0");
    return;
  }
  setFormula(calcResult);
};

// for debugging - currently not displayed
document.getElementById("debugButton").onclick = () => {
  // const currentFormula = getFormula();
  console.log("-------DEBUGGING--------")
  const formulaObject = splitFormulaToPartsAndReturnObject(getFormula());
  console.log("ob", formulaObject);
  console.log("calc", calculate(formulaObject));
  // console.log(!/\d/.test(getLastCharOfFormula()));
};