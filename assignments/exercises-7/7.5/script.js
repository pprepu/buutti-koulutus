const myKey = "c3a0092f";

async function getData (title, year) {
  const data = await fetch(`http://www.omdbapi.com/?apikey=${myKey}&s=${title}&y=${year}`);
  const json = await data.json();
  return json;
}

const createLinkElement = (link, text) => {
  const linkElement = document.createElement("a");
  linkElement.href = link;
  linkElement.innerText = text;
  return linkElement;
};

const createListElement = children => {
  const listElement = document.createElement("li");
  children.forEach(child => listElement.appendChild(child));
  return listElement;
};

document.querySelector("button").addEventListener("click", async () => {
  const title = document.querySelector("#title").value;
  const year = document.querySelector("#year").value;
  
  try {
    const result = await getData(title, year);
    const resultsList = document.querySelector(".results");

    while (resultsList.firstChild) {
      resultsList.removeChild(resultsList.lastChild);
    }

    for (const data of result.Search) {
      const paragraph = document.createElement("p");
      paragraph.innerText = `title: ${data.Title}, year: ${data.Year} `;

      const poster = document.createElement("img");
      poster.src = data.Poster;
      resultsList.appendChild(
        createListElement(
          [
            paragraph, 
            createLinkElement(`http://www.imdb.com/title/${data.imdbID}`, "link"),
            poster
          ]
        )
      );
    }
  } catch (error) {
    console.log(error);
  }
});