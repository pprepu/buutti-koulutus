import jwt from "jsonwebtoken";

const secret = "SEEEEEEEEEEEEEEKRETTTTTTTTTTTTTTTTTT";

const decodeToken = token => {
  try {
    const decodedToken = jwt.verify(token, secret);
    return decodedToken;
  } catch (error) {
    console.log("Unable to decode token");
    return null;
  }
};

const validToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1laXRzaSAiLCJpYXQiOjE2NjkyMDI2MTEsImV4cCI6MTY2OTIwMzUxMX0.xcdawPkn5Wqhdn60asfYXkbn--IzQhGKeTWyFd9RP_w";
console.log(decodeToken(validToken));

const invalidToken = "isthisatoken?";
console.log(decodeToken(invalidToken));