module.exports = async function (context, req) {
  context.log("function triggered");

  let input = "";
  if (req.body && req.body.input) {
    input = req.body.input;
  }

  if (typeof input === "string") {
    input = input.toUpperCase();
  }
  context.res = {
    // status: 200, /* Defaults to 200 */
    body: input
  };
};