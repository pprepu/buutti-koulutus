import express from "express";
const server = express();

server.get("/", (req, res) => {
  res.send("My web application!");
});

const PORT = process.env.PORT || 3000;

server.listen(PORT);