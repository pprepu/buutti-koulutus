const friendsUrl = "https://pprepu-webapp-friends.azurewebsites.net/friends";
const createRow = friend => {
  const tableRow = document.createElement("tr");
  for (const key in friend) {
    const tableCell = document.createElement("td");
    tableCell.innerText = friend[key];
    tableRow.appendChild(tableCell);
  }
  return tableRow;
};

const createTh = text => {
  const tableTh = document.createElement("th");
  tableTh.innerText = text;
  return tableTh;
};

const createTable = friends => {
  const tableHeaders = ["username", "name", "email"];
  const table = document.createElement("table");
  const thead = document.createElement("thead");
  
  tableHeaders.forEach(header => thead.appendChild(createTh(header)));
  table.appendChild(thead);

  const tbody = document.createElement("tbody");
  friends.forEach(friend => tbody.appendChild(createRow(friend)));
  table.appendChild(tbody);

  return table;
};

const addFriendsToDom = friends => {
  const friendsDiv = document.querySelector(".friendsDiv");
  if (friends.length === 0) {
    const noFriendsParagraph = document.createElement("p");
    noFriendsParagraph.innerText = "no friends :(";
    friendsDiv.appendChild(noFriendsParagraph);
    return;
  }

  friendsDiv.appendChild(createTable(friends));
};
const fetchFriends = async () => {
  try {
    const response = await fetch(friendsUrl);
    const friends = await response.json();
    
    addFriendsToDom(friends);
  } catch (error) {
    console.log(error);
  }

};

window.onload = fetchFriends();