import express from "express";
const router = express.Router();

let friends = [];

router.get("/", (req, res) => {
  res.status(200).send(friends);
});

router.post("/", (req, res) => {
  const { username, name, email } = req.body;

  if (!username || !name || !email) {
    return res.status(400).send("could not add user: necessary fields not provided");
  }

  friends = friends.concat({ username, name, email });
  res.status(201).send();
});

export default router;