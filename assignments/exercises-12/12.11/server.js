import express from "express";
import friendsRouter from "./friendsRouter.js";

const server = express();
server.use(express.static("public"));
server.use(express.json());
server.use("/friends", friendsRouter);

export default server;