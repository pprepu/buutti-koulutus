const https = require("https");
const httpsAgent = new https.Agent();
httpsAgent.maxSockets = 200;

const defaultFriend = {
  username: "apiNOTworking",
  name: "usingDefaultValues",
  email: "namefake@down.sad" 
};

const getNewFriendAsPromise = () => new Promise((resolve, reject) => {
  const optionForRequest = {
    hostname: "api.namefake.com",
    path: "/",
    port: 443,
    agent: httpsAgent
  };
  const getRequest = https.get(optionForRequest, res => {
    res.setEncoding("utf8");
    res.on("data", data => {
      resolve(JSON.parse(data));
    });
  });
  getRequest.on("error", error => {
    reject({error});
  });
  getRequest.end();
});

const postNewFriend = (username, name, email) => {
  try {
    const newFriendAsJson = JSON.stringify({ username, name, email });
    const post_options = {
      method: "POST",
      hostname: "pprepu-webapp-friends.azurewebsites.net",
      path: "/friends",
      agent: httpsAgent,
      headers: {
        "Content-Type": "application/json",
        "Content-Length": newFriendAsJson.length
      },
    };
    
    const postRequest = https.request(post_options).on("error", error => {
      console.log(error);
    });
  
    postRequest.write(newFriendAsJson);
    postRequest.end();
  } catch (error) { // if GET request to namefake returns something JSON.stringify struggles to work with
    const newFriendAsJson = JSON.stringify({ ...defaultFriend });
    const post_options = {
      method: "POST",
      hostname: "pprepu-webapp-friends.azurewebsites.net",
      path: "/friends",
      agent: httpsAgent,
      headers: {
        "Content-Type": "application/json",
        "Content-Length": newFriendAsJson.length
      },
    };
    
    const postRequest = https.request(post_options).on("error", error => {
      console.log(error);
    });
  
    postRequest.write(newFriendAsJson);
    postRequest.end();
  }
};

const createNewFriend = async () => {
  try {
    const newFriend = await getNewFriendAsPromise();
    postNewFriend(newFriend.username, newFriend.name, `${newFriend.email_u}@${newFriend.email_d}`);
  } catch (error) { // if GET request to namefake throws an error
    postNewFriend(defaultFriend.username, defaultFriend.name, defaultFriend.email);
  }
};

module.exports = async function () {
  createNewFriend();
};