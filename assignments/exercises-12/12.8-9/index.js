import express from "express";
const server = express();

server.use(express.static("public"));

const createRandomNumber = (min, max, int) => {
  const distance = max - min;
  return int
    ? Math.floor(Math.random() * (distance + 1)) + min
    : Math.random() * distance + min;
};

server.get("/random", (req, res) => {
  let { min, max, int } = req.query;
  if (min === undefined || max === undefined || int === undefined) {
    return res.send("Some parameters not found. Check root path for help.");
  }

  min = Number(min);
  max = Number(max);
  int = int === "true" 
    ? true 
    : int === "false" 
      ? false 
      : null;

  if (isNaN(min) || isNaN(max) || int === null) {
    return res.send("Some parameters could not be parsed to correct types. Check root path for help.");
  }
  if (min > max) {
    return res.send("Min has to be lower than max");
  }

  if ( (min % 1 !== 0 || max % 1 !== 0) && int ) {
    return res.send("If min or max are not integers, int has to be set to 'false'");
  }
  res.send(`Random number generated from min: ${min}, max: ${max}, integer: ${int} --- ${createRandomNumber(min, max, int)}`);
});

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
  console.log("listening to port", PORT);
});