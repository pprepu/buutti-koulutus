const createRandomNumber = (min, max, int) => {
  const distance = max - min;
  return int
    ? Math.floor(Math.random() * (distance + 1)) + min
    : Math.random() * distance + min;
};

const checkIfParametersHaveErrors = (min, max, int) => {

  if (isNaN(min) || isNaN(max) || int === null) {
    return "Some parameters could not be parsed to correct types. Check root path for help.";
  }
  if (min > max) {
    return "Min has to be lower than max";
  }

  if ( (min % 1 !== 0 || max % 1 !== 0) && int ) {
    return "If min or max are not integers, int has to be set to 'false'";
  }

  return false;
};

module.exports = async function (context, req) {
  context.log("JavaScript HTTP trigger function processed a request.");

  let { min, max, int } = req.query;

  min = Number(min);
  max = Number(max);
  int = int === "true" 
    ? true 
    : int === "false" 
      ? false 
      : null;

  const errorsWithParameters = checkIfParametersHaveErrors(min, max, int);

  const responseMessage = errorsWithParameters
    ? errorsWithParameters
    : `Random number generated from min: ${min}, max: ${max}, integer: ${int} --- ${createRandomNumber(min, max, int)}`;

  context.res = {
    // status: 200, /* Defaults to 200 */
    body: responseMessage
  };
};