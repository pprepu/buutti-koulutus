// helper for getWordCountsAndModifyDom - returns an object with words and their counts
const getWordCounts = array => {
  const wordCounts = {};

  for (let word of array) {
    // ignoring case
    word = word.toLowerCase();
    if (!wordCounts[word]) {
      wordCounts[word] = 0;
    }

    ++wordCounts[word];
  }
  return wordCounts;
};
// helper for getWordCountsAndModifyDom
const transformObjectToSortedArray = object => {
  const arrayOfObjects = Object.keys(object).map(key => { 
    return { key, value: object[key] };
  });
  arrayOfObjects.sort((object1, object2) => object2.value - object1.value); // descending order
  return arrayOfObjects;
};

const getWordCountsAndModifyDom = allWords => {
  const wordsAsObjectsSorted = transformObjectToSortedArray(getWordCounts(allWords));
  
  const orderedList = document.createElement("ol");
  orderedList.innerText = "words and their counts:";

  for (const object of wordsAsObjectsSorted) {
    const listElement = document.createElement("li");
    listElement.innerText =`${object.key}: ${object.value}`;
    orderedList.appendChild(listElement);
  }

  const outputDiv = document.querySelector(".output");
  outputDiv.appendChild(orderedList);
};

const countTotalWordCountAndModifyDom = allWords => {
  const paragraph = document.createElement("p");
  paragraph.innerText = "total word count: " + allWords.length;

  const outputDiv = document.querySelector(".output");
  outputDiv.appendChild(paragraph);
};

const countAverageLengthOfWordsAndModifyDom = allWords => {
  const averageLength = allWords.reduce((acc, cur) => acc + cur.length, 0) / allWords.length;
  const paragraph = document.createElement("p");
  paragraph.innerText = "average length of the words: " + Math.round(averageLength);
  
  const outputDiv = document.querySelector(".output");
  outputDiv.appendChild(paragraph);
};
 
const analyzeButton = document.querySelector(".analyzeButton");
analyzeButton.addEventListener("click", () => {
  const textarea = document.querySelector("textarea");

  const textWithoutSpecialCharacters = textarea.value
    .replaceAll(/\n/g, " ") // replacing newline characters with empty spaces to prevent some words clipping into each other when splitting to an array with (" ")
    .replaceAll(/[.?!,"-_():;“—”]/g, "");
  const allWords = textWithoutSpecialCharacters.split(" ").filter(word => word); // filtering remaining empty spaces out
  countTotalWordCountAndModifyDom(allWords);
  countAverageLengthOfWordsAndModifyDom(allWords);
  getWordCountsAndModifyDom(allWords);
});