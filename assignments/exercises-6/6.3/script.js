// state - original values
let rows = 3;
let playerTurn = "X";
let gameOver = false;

// helper for handlePlayerTurn
const changePlayerTurn = () => {
  playerTurn = playerTurn === "X" ? "O" : "X";
};
// helper for handlePlayerTurn
const endCurrentGame = () => {
  gameOver = true;
  const cells = document.querySelectorAll(".grid > .cell");
  cells.forEach(cell => cell.style.backgroundColor = "red");
};

// listener given to all cells in generateGrid
const handlePlayerTurn = (event) => {
  const cell = event.target;
  if (!gameOver && cell.innerText === "") {
    const txt = document.createTextNode(playerTurn);
    changePlayerTurn();
    cell.appendChild(txt);
    // check if someone has won after every player turn and end the game if necessary
    if (isGameOver(rows)) {
      endCurrentGame();
    }
  }
};

// helper for generateGrid - makes the grid have the right amount of rows/columns
const createStyleForGrid = rows => {
  let style = "";
  for (let i = 0; i < rows; i++) {
    style += "auto ";
  }
  return style;
};
// generates dynamically the grid used for playing
const generateGrid = rows => {
  const grid = document.querySelector(".grid");
  grid.style["grid-template-columns"] = createStyleForGrid(rows);
  while (grid.firstChild) {
    grid.removeChild(grid.lastChild);
  }

  for (let i = 0; i < rows * rows; i++) {
    let cell = document.createElement("div");
    cell.classList.add("cell");
    cell.addEventListener("click", handlePlayerTurn);
    grid.appendChild(cell);
  }
};
// generates a new grid with a dynamic amount of rows, resets other state values
const resetApp = rowsNew => {
  generateGrid(rowsNew);
  rows = rowsNew;
  playerTurn = "X";
  gameOver = false;
};

// adding eventlisteners for the 3 buttons
const buttonMinus = document.querySelector(".minus");
buttonMinus.addEventListener("click", () => rows > 3 && resetApp(rows - 1));

const buttonPlus = document.querySelector(".plus");
buttonPlus.addEventListener("click", () => resetApp(rows + 1));

const buttonReset = document.querySelector(".reset");
buttonReset.addEventListener("click", () => resetApp(3));

// helper for isGameOver
const checkRows = (cells, rows) => {
  // since the 2-D grid is represented by a 1-D array, there's some semi trickery needed
  // basically the outer loop controls the starting points of all the rows
  //  - first row starts on index 0, second row on 0 + length of a row etc.
  //    - length of a row is always the same as the number of rows (because the grid is a square)
  for (let startingIndex = 0; startingIndex < cells.length; startingIndex += rows) {
    let currentRow = "";
    // inner loops controls everything happening on one row
    // here, I'm collecting all the values to one string
    for (let rowIndex = startingIndex; rowIndex < rows + startingIndex; rowIndex++) {
      currentRow += cells[rowIndex];
    }
    // checking first that there are no empty values on a row (there cannot be a win)
    // checking then if they are all the same (someone has won)
    if (currentRow.length === rows && currentRow.split("").every(char => char === currentRow[0])) {
      return true;
    }
  }
  // if there is no winning row found, return false by default
  return false;
};

// helper for isGameOver
const checkColumns = (cells, columns) => {
  // same problems and logic as in the checkRows function, just transferred to handling columns
  for (let startingIndex = 0; startingIndex < columns; startingIndex ++) {
    let currentColumn = "";
    for (let columnIndex = startingIndex; columnIndex < cells.length; columnIndex += columns) {
      currentColumn += cells[columnIndex];
    }
    if (currentColumn.length === rows && currentColumn.split("").every(char => char === currentColumn[0])) {
      return true;
    }
  }
  return false;
};

// helper for isGameOver
const checkDiagonals = (cells, rows) => {
  // only have to check the 2 longest diagonals
  // first one starting from [x: 0, y: 0] to [x: rows-1, y: rows-1]
  let currentDiagonal = "";
  for (let index = 0; index < cells.length; index += rows + 1) {
    currentDiagonal += cells[index];
  }
  if (currentDiagonal.length === rows && currentDiagonal.split("").every(char => char === currentDiagonal[0])) {
    return true;
  }

  // second diagonal from [x: rows-1, y: 0] to [x: 0, y: rows-1]
  currentDiagonal = "";
  for (let index = rows - 1; index < cells.length; index += rows - 1) {
    currentDiagonal += cells[index];
  }

  if (currentDiagonal.length === rows && currentDiagonal.split("").every(char => char === currentDiagonal[0])) {
    return true;
  }

  return false;

};
// helper for handlePlayerTurn
const isGameOver = rows => {
  // map cells to their innerText (= "X" || "O" || "")
  const cells = document.querySelectorAll(".grid > .cell");
  const cellsMappedToText = [];
  for (const cell of cells) {
    cellsMappedToText.push(cell.innerText);
  }
  return checkRows(cellsMappedToText, rows)
    || checkColumns(cellsMappedToText, rows)
    || checkDiagonals(cellsMappedToText, rows);
};

const buttonDebug = document.querySelector(".debug");
buttonDebug.addEventListener("click", () => {
  const cells = document.querySelectorAll(".grid > .cell");
  // map cell objects into a new array
  const cellsMappedToText = [];
  for (const cell of cells) {
    cellsMappedToText.push(cell.innerText);
  }
  console.log(cellsMappedToText);
  console.log("checkRows", checkRows(cellsMappedToText, rows));
  console.log("checkColumns", checkColumns(cellsMappedToText, rows));
  console.log("checkDiagonals", checkDiagonals(cellsMappedToText, rows));

});

// generate intial grid
generateGrid(rows);