let gameOver = false;
const correctOrder = [...Array(16).keys(), 0].slice(1).join(""); //1234567891011121314150

const cells = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 15];
const neighboursByIndex = {
  0: [1, 4],
  1: [0, 2, 5],
  2: [1, 3, 6],
  3: [2, 7],
  4: [0, 5, 8],
  5: [1, 4, 6, 9],
  6: [2, 5, 7, 10],
  7: [3, 6, 11],
  8: [4, 9, 12],
  9: [5, 8, 10, 13],
  10: [6, 9, 11, 14],
  11: [7, 10, 15],
  12: [8, 13],
  13: [9, 12, 14],
  14: [10, 13, 15],
  15: [11, 14]
};

// helper for shuffleArrayInPlace AND cellHandleClick
const swapTwoCells = (array, index1, index2) => {
  [array[index2], array[index1]] = 
      [array[index1], array[index2]];
};
// shuffling the cells for random grid positions
const shuffleArrayInPlace = array => {
  let randomIndex = null;
  let currentIndex = array.length;

  while (currentIndex > 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    swapTwoCells(cells, --currentIndex, randomIndex);
  }
};

const isGameOver = () => {
  if (cells.join("") === correctOrder) {
    gameOver = true;
    const winnerPrompt = document.querySelector(".winnerPrompt");
    winnerPrompt.classList.toggle("invisible");
  }
};
// eventlistener for cells in grid
const cellHandleClick = index => {
  if (gameOver) {
    return;
  }
  for (const neighbourIndex of neighboursByIndex[index]) {
    if (!cells[neighbourIndex]) {
      swapTwoCells(cells, index, neighbourIndex);
      isGameOver();
      generateGrid();
      return;
    }
  }
};

const generateGrid = () => {
  const grid = document.querySelector(".grid");
  while (grid.firstChild) {
    grid.removeChild(grid.lastChild);
  }

  for (let i = 0; i <= 15; i++) {
    const cell = document.createElement("div");
    cell.classList.add("cell");
    // cell.innerText = cells[i] || "";
    cell.addEventListener("click", () => cellHandleClick(i));
    
    if (!(cells[i] === 0 && !gameOver)) {
      const image = document.createElement("img");
      image.src = `./images/${cells[i]}.png`;
      cell.appendChild(image);
    }
    
    grid.appendChild(cell);
  }
};

// for debugging win conditions, comment out shuffleArrayInPlace below
shuffleArrayInPlace(cells);
generateGrid();