const addTodo = event => {
  event.preventDefault();
  const input = document.querySelector("input");

  if (event.key === "Enter") {
    
    const listElement = document.createElement("li");
    listElement.innerText = input.value;
    listElement.classList.add("listElement");

    listElement.addEventListener("click", () => {
      listElement.classList.toggle("clicked");
    });

    const todoList = document.querySelector(".todoList");
    todoList.appendChild(listElement);
    input.value = "";
  } else {
    input.value += event.key;
  }
};

const todoButton = document.querySelector(".todoButton");
todoButton.addEventListener("click", () => {
  const todoList = document.querySelector(".todoList");
  // keeping track of elements to keep in the list
  // I know there HAS to be a better way to do this
  const todoElementsKept = [];
  for (let i = 0; i < todoList.children.length; i++) {
    const listElement = todoList.children[i];
    // transforming element to an array so I can use array methods like .includes
    const elementAsArray = Array.from(listElement.classList);
    if (!elementAsArray.includes("clicked")) {
      todoElementsKept.push(listElement);
    }
  }
  // deleting all previous elements from the list
  while (todoList.firstChild) {
    todoList.removeChild(todoList.lastChild);
  }
  // adding back the ones that aren't finished
  todoElementsKept.forEach(element => todoList.appendChild(element));
});