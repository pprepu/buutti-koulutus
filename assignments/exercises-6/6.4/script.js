// get the text from inside <p> and split it to an array in sentences
const loremIpsumSplit = document
  .querySelector("p")
  .innerText
  .split(". ");

// delete the current p
const container = document.querySelector(".loremContainer");
while (container.firstChild) {
  container.removeChild(container.lastChild);
}
// go through all sentences, modify the longer words when needed and create sentences as new paragraphs
for (let i = 0; i < loremIpsumSplit.length; i++) {
  let sentence = loremIpsumSplit[i]
    .replace(/\w{7,}/g, word => "<span style='color: yellow'>" + word + "</span>");
  // add "." back if needed (since they were taken out when .splitting)
  if (i < loremIpsumSplit.length - 1) {
    sentence += ".";
  }
  const paragraph = document.createElement("p");
  paragraph.innerHTML = sentence;
  container.appendChild(paragraph);
}