import express from "express";
import dao from "./db/productDao.js";

const router = express.Router();

router.get("/", async (req, res) => {
  const allProducts = await dao.findAll();
  res.send(allProducts.rows);
});

router.post("/", async (req, res) => {
  const product = req.body;
  if (!product.name || !product.price) {
    return res.status(403).send("name or price not sent");
  }
  const result = await dao.insertProduct(product);
  const storedProduct = result.rows[0];
  res.status(201).send(storedProduct);
});

router.get("/:id", async (req, res) => {
  const result = await dao.findOne(req.params.id);
  const product = result.rows[0];
  res.send(product);
});

router.put("/:id", async (req, res) => {
  const product = { id: req.params.id, ...req.body };
  if (!req.params.id || !product.name || !product.price) {
    return res.status(403).send("id, name or price not sent");
  }
  await dao.updateProduct(product);
  res.status(204).send();
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  await dao.deleteOne(id);
  res.status(204).send();
});

export default router;