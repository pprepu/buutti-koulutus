import { createProductTable } from "./db/db.js";
import server from "./server.js";

createProductTable();

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
  console.log("listening to port" , PORT);
  console.log(process.env.PG_PASSWORD);
});