const createProductTable = `
  CREATE TABLE IF NOT EXISTS "products" (
    "id" SERIAL,
    "name" VARCHAR(100) NOT NULL,
    "price" INTEGER NOT NULL,
    PRIMARY KEY ("id")
  );
`;

const findAll = "SELECT id, name, price FROM products";

const insertProduct = "INSERT INTO products (name, price) VALUES ($1, $2)";

const findOne = "SELECT * FROM products WHERE id = $1";

const updateProduct = "UPDATE products SET name = $2, price = $3 WHERE id = $1";

const deleteOne = "DELETE FROM products WHERE id = $1";

export default { 
  createProductTable,
  findAll,
  insertProduct,
  findOne,
  updateProduct,
  deleteOne
};