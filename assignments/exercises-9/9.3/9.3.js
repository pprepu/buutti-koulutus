import express from "express";
const PORT = 3000;

const server = express();

let counter = 0;

server.get("/counter", (req, res) => {
  if (req.query.number) {
    counter = req.query.number;
  } else {
    counter++;
  }
  res.send(`<h1> Counter: ${counter}</h1>`);
});

server.listen(PORT, () => {
  console.log("listening to " + PORT);
});