import express from "express";
import { logger, unknownEndpoint, authenticate } from "./middlewares/middlewares.js";
import studentRouter from "./studentRouter.js";
import userRouter from "./userRouter.js";

const server = express();
server.use(express.json());
server.use("/", express.static("public"));
server.use(logger);

server.use("/students", authenticate, studentRouter);
server.use("", userRouter);

server.use(unknownEndpoint);

export default server;