import express from "express";
import { isAdmin } from "./middlewares/middlewares.js";

const router = express.Router();
let students = [];

router.get("/", (_req, res) => {
  res.send(students.map(student => student.id));
});

router.get("/:id", (req, res) => {
  const id = req.params.id;
  const student = students.find(student => student.id === id);
  if (!student) {
    res.status(404).end();
  } else {
    res.send(student);
  }
});

router.post("/", isAdmin, (req, res) => {
  const { id, name, email } = req.body;
  if (!id || !name || !email) {
    res.status(400).send({error: "Name, user or email missing from student"});
    return;
  }

  students = [...students, { id, name, email }];
  res.status(201).end();
});

router.put("/:id", isAdmin, (req, res) => {
  const { name, email } = req.body;
  if (!name && !email) {
    res.status(400).send({error: "Name and email missing from request"});
    return;
  }
  const id = req.params.id;
  const studentFound = students.find(student => student.id === id);
  if (!studentFound) {
    res.status(404).send();
  } else {
    students = students.map(student => student.id !== id ? student : {id, name: name || studentFound.name, email: email || studentFound.email });
    res.status(204).send();
  } 
});

router.delete("/:id", isAdmin, (req, res) => {
  const id = req.params.id;
  const studentFound = students.find(student => student.id === id);
  if (!studentFound) {
    res.status(404).send();
  } else {
    students = students.filter(student => student.id !== id);
    res.status(204).send();
  } 
});

export default router;