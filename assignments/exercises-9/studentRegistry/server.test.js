import "dotenv/config";
import request from "supertest";
import server from "./server.js";
import jwt from "jsonwebtoken";

// beforeEach(async () => {
//   await request()
// })

describe("server", () => {
  it("registering with a username and password returns the correct status code and a valid jwt token with username property", async () => {
    const response = await request(server)
      .post("/register")
      .send({ username: "testaaja", password: "passu" })
      .expect(200);

    try {
      const token = jwt.verify(response.text, process.env.SECRET);
      expect(token.username).toEqual("testaaja");
    } catch (error)  {
      console.error("error in decoding token");
      expect(true).toBe(false);
    }
  });

  it("registering without a password returns a correct status code and text", async () => {
    const response = await request(server)
      .post("/register")
      .send({ username: "testaaja" })
      .expect(402);

    expect(response.text).toEqual("missing user info");
  });

  it("registering without a username returns a correct status code and text", async () => {
    const response = await request(server)
      .post("/register")
      .send({ password: "passu" })
      .expect(402);

    expect(response.text).toEqual("missing user info");
  });

  it("logging in with a nonexisting username returns a correct status code and text", async () => {
    const response = await request(server)
      .post("/login")
      .send({ username: "whoisthis", password: "passu" })
      .expect(401);
    
    expect(response.text).toEqual("access denied");
  });

  it("logging in with a correct username and password returns correct status code and a valid jwt token", async () => {
    const response = await request(server)
      .post("/login")
      .send({ username: "testaaja", password: "passu" })
      .expect(200);
    
    try {
      const token = jwt.verify(response.text, process.env.SECRET);
      expect(token.username).toEqual("testaaja");
    } catch (error)  {
      console.error("error in decoding token");
      expect(true).toBe(false);
    }
  });
  
  it("logging in with an existing user but incorrect password returns a correct status code and text", async () => {
    const response = await request(server)
      .post("/login")
      .send({ username: "testaaja", password: "eioleoikeapassu" })
      .expect(401);
    
    expect(response.text).toEqual("access denied");
  });
});