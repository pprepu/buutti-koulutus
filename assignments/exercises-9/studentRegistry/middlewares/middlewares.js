import "dotenv/config";
import jwt from "jsonwebtoken";

export const logger = (req, _res, next) => {
  if (process.env.NODE_ENV !== "test") {
    console.log("time:", new Date().toUTCString());
    console.log("method:", req.method);
    console.log("endpoint:", req.url);

    if (Object.keys(req.body).length > 0) {
      console.log("body:", req.body);
    }
  }
  next();
};

export const unknownEndpoint = (_req, res) => {
  res.status(404).send({ error: "unknown endpoint "});
};

export const authenticate = (req, res, next) => {
  const auth = req.get("Authorization");
  if (!auth || !auth.startsWith("Bearer ")) {
    return res.status(401).send("token invalid");
  }

  const token = auth.substring(7);
  try {
    const decodedToken = jwt.verify(token, process.env.SECRET);
    req.user = decodedToken;
    next();
  } catch (error) {
    res.status(401).send("token invalid");
  }
};

export const isAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    res.status(401).send("access denied");
  }
};