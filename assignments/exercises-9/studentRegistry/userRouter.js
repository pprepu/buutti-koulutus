import "dotenv/config";
import express from "express";
import argon2 from "argon2";
import jwt from "jsonwebtoken";

const createTokenForUser = (username, isAdmin = false) => jwt.sign({ username, isAdmin }, process.env.SECRET);

let users = [];
const router = express.Router();

router.use(express.json());
router.post("/register", (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(402).send("missing user info");
  }

  argon2.hash(password)
    .then(passwordHash => {
      console.log(passwordHash);
      users = users.concat({ username, passwordHash });
      res.status(201).send(createTokenForUser(username));
    })
    .catch(error => {
      console.error(error);
      res.status(403).send("Something bad happened");
    });
});

router.post("/login", (req, res) => {
  const { username, password } = req.body;

  const userFound = users.find(user => user.username === username);
  if (!userFound) {
    return res.status(401).send("access denied");
  }

  argon2.verify(userFound.passwordHash, password)
    .then(match => match ? res.status(200).send(createTokenForUser(username)) : res.status(401).send("access denied"))
    .catch(error => {
      console.error(error);
      res.status(403).send("Something bad happened");
    });
});

router.post("/admin" , (req, res) => {
  const { username, password } = req.body;
  if (username !== process.env.ADMIN_USERNAME) {
    return res.status(401).send("access denied");
  }
  console.log(process.env.ADMIN_PASSWORD);
  argon2.verify(process.env.ADMIN_PASSWORD, password)
    .then(match => match ? res.status(200).send(createTokenForUser(process.env.ADMIN_USERNAME, true)) : res.status(401).send())
    .catch(error => {
      console.error(error);
      res.status(403).send("Something bad happened");
    });

});
export default router;