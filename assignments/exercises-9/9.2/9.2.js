import express from "express";
const PORT = 3000;

const server = express();

server.get("/endpoint2", (_req, res) => {
  res.send("Endpoint 2 works!");
});

server.get("/", (_req, res) => {
  res.send("Hello world!");
});

server.listen(PORT, () => {
  console.log("listening to port: " + PORT);
});