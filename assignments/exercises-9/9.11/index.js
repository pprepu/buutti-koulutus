import express from "express";
import { numberParser } from "./middlewares.js";
const server = express();
const PORT = 3000;

server.use(numberParser);

server.get("/add", (req, res) => {
  res.send("" + req.numbers.reduce((acc, cur) => acc + cur, 0));
});

server.get("/subtract", (req, res) => {
  res.send("" + req.numbers.reduce((acc, cur) => acc - cur));
});

server.get("/multiply", (req, res) => {
  res.send("" + req.numbers.reduce((acc, cur) => acc * cur));
});

server.get("/divide", (req, res) => {
  if (req.numbers.length > 1 && req.numbers.slice(1).some(number => number === 0)) {
    return res.status(400).send({error: "cannot divide by zero"});
  }
  res.send("" + req.numbers.reduce((acc, cur) => acc / cur));
});

server.listen(PORT, () => {
  console.log("listening to port", PORT);
});