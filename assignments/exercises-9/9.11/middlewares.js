export const numberParser = (req, res, next) => {
  if (!req.query.numbers) {
    return res.send("0");
  }
  const numbers = req.query.numbers.split(",").map(number => Number(number));

  if (numbers.some(number => isNaN(number))) {
    return res.status(400).send({error: "Some query values could not be parsed into numbers"});
  } 
  req.numbers = numbers;
  next();
};