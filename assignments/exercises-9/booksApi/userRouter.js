import "dotenv/config";
import express from "express";
import jwt from "jsonwebtoken";
import argon2 from "argon2";
const router = express.Router();

let users = [];

router.post("/register", (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).send({ error: "username or password missing" });
  }

  if (username === process.env.ADMIN_USERNAME) {
    return res.status(403).send({ error: "username not allowed" });
  }

  if (users.some(user => user.username === username)) {
    return res.status(403).send({ error: "username not allowed" });
  }

  const token = jwt.sign({ username }, process.env.SECRET);
  argon2.hash(password).then(passwordHash => {
    const newUser = { username, passwordHash };
    users = users.concat(newUser);
    res.status(201).send(token);
  });
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;

  if (username === process.env.ADMIN_USERNAME) {
    const match = await argon2.verify(process.env.ADMIN_PASSWORDHASH, password);
    if (!match) {
      return res.status(401).send({ error: "access denied" });
    }
    const tokenForAdmin = jwt.sign({ username, isAdmin: true }, process.env.SECRET);
    return res.status(200).send(tokenForAdmin);
  }

  const userFound = users.find(user => user.username === username);
  if (!userFound) {
    return res.status(401).send({ error: "access denied" });
  }

  const token = jwt.sign({ username }, process.env.SECRET);

  const match = await argon2.verify(userFound.passwordHash, password);
  if (!match) {
    return res.status(401).send({ error: " access denied" });
  }

  return res.status(200).send(token);
});


export default router;