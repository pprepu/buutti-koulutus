import express from "express";
import helmet from "helmet";
import { logger, unknownEndpoint, authenticate } from "./middlewares.js";
import bookRouter from "./bookRouter.js";
import userRouter from "./userRouter.js";

const baseUrl = "/api/v1";
const server = express();

server.use(helmet());

server.use(express.static("public"));
server.use(express.json());
server.use(logger);

server.use(baseUrl + "/books", authenticate, bookRouter);
server.use(baseUrl + "/users", userRouter);

server.use(unknownEndpoint);

export default server;