import "dotenv/config";
import server from "./server.js";

server.listen(process.env.PORT, () => {
  console.log("listening to port", process.env.PORT);
});