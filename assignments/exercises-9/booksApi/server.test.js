import "dotenv/config";
import jwt from "jsonwebtoken";
import request from "supertest";
import server from "./server.js";
const baseUrlForUsers = "/api/v1/users";
const baseUrlForBooks = "/api/v1/books";

describe("user", () => {
  it("can be created with a unique username and password and server returns a valid token with the correct status code", async () => {
    const newUser = {
      username: "jarmo",
      password: "jorma"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/register")
      .send(newUser)
      .expect(201);

    try {
      const decodedToken = jwt.verify(response.text, process.env.SECRET);
      expect(decodedToken.username).toEqual("jarmo");
      expect(decodedToken.isAdmin).toBe(undefined);
    } catch (error) {
      console.log(error);
      expect(true).toBe(false);
    }
  });

  it("cannot be created with a duplicate username and server returns an error message with the correct status code", async () => {
    const newUser = {
      username: "jarmo",
      password: "jorma"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/register")
      .send(newUser)
      .expect(403);

    expect(JSON.parse(response.text).error).toEqual("username not allowed");
  });

  it("cannot be created with username 'admin' and server returns an error message with the correct status code", async () => {
    const newUser = {
      username: "admin",
      password: "adminPassword"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/register")
      .send(newUser)
      .expect(403);

    expect(JSON.parse(response.text).error).toEqual("username not allowed");
  });

  it("cannot be created without providing a username and server returns an error message with the correct status code ", async () => {
    const newUser = {
      password: "pa55word"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/register")
      .send(newUser)
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("username or password missing");
  });

  it("cannot be created without providing a password and server returns an error message with the correct status code ", async () => {
    const newUser = {
      username: "testaaja"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/register")
      .send(newUser)
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("username or password missing");
  });

  it("can log in with the correct password if the user exists and server returns a valid token with the correct status code", async () => {
    const userLoggingIn = {
      username: "jarmo",
      password: "jorma"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/login")
      .send(userLoggingIn)
      .expect(200);

    try {
      const decodedToken = jwt.verify(response.text, process.env.SECRET);
      expect(decodedToken.username).toEqual("jarmo");
      expect(decodedToken.isAdmin).toBe(undefined);
    } catch (error) {
      console.log(error);
      expect(true).toBe(false);
    }
  });
  it("cannot log in if the username does not exist and server returns an error message with the correct status code", async () => {
    const userLoggingIn = {
      username: "whoAmI",
      password: "passvordi"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/login")
      .send(userLoggingIn)
      .expect(401);

    expect(JSON.parse(response.text).error).toEqual("access denied");
  });
  it("cannot log in if with an incorrect password and server returns an error message with the correct status code", async () => {
    const userLoggingIn = {
      username: "jorma",
      password: "wrongPassword"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/login")
      .send(userLoggingIn)
      .expect(401);

    expect(JSON.parse(response.text).error).toEqual("access denied");
  });
  it("can log in as admin and server returns a modified token with isAdmin property and a correct status code", async () => {
    const userLoggingIn = {
      username: "admin",
      password: process.env.ADMIN_PASSWORD
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/login")
      .send(userLoggingIn)
      .expect(200);

    try {
      const decodedToken = jwt.verify(response.text, process.env.SECRET);
      expect(decodedToken.username).toEqual("admin");
      expect(decodedToken.isAdmin).toBe(true);
    } catch (error) {
      console.log(error);
      expect(true).toBe(false);
    }
  });
  it("cannot log in as admin with incorrect password and server returns an error message with the correct status code", async () => {
    const userLoggingIn = {
      username: "admin",
      password: "wrongPasswordForAdmin"
    };
    const response = await request(server)
      .post(baseUrlForUsers + "/login")
      .send(userLoggingIn)
      .expect(401);

    expect(JSON.parse(response.text).error).toEqual("access denied");
  });
});

// oops, here are some extra tests my hazy brain thought I was supposed to make
describe("books", () => {
  const createAdminToken = () => jwt.sign({ username: "admin", isAdmin: true }, process.env.SECRET);
  const createTokenForUsername = username => jwt.sign({ username }, process.env.SECRET);
  const createBearerStringForHeader = token => "Bearer " + token;

  const bookToBeCreated = {
    id: 1,
    name: "Oikeusjuttu",
    author: "Franz Kafka",
    read: true
  };

  const bookToBeEdited = {
    name: "Mies joka katosi",
    author: "F. Kafka",
    read: true
  };

  it("cannot be listed without proper authentication and server returns an error message with the correct status code", async () => {
    const response = await request(server)
      .get(baseUrlForBooks)
      .expect(401);

    expect(JSON.parse(response.text).error).toEqual("token invalid");
  });

  it("can be listed when a valid token is sent and server returns an empty list (at startup) with the correct status code", async () => {
    const response = await request(server)
      .get(baseUrlForBooks)
      .set("Authorization", createBearerStringForHeader(createTokenForUsername("testUser")))
      .expect(200);

    const booksReturned = JSON.parse(response.text);
    expect(booksReturned.length).toBe(0);
    expect(typeof booksReturned).toBe("object");
  });

  // post requests
  it("cannot be created without an admin token and server returns an error message with the correct status code", async () => {
    const book = {
      id: 1,
      name: "Oikeusjuttu",
      author: "Franz Kafka",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createTokenForUsername("testUser")))
      .expect(401);

    expect(JSON.parse(response.text).error).toEqual("access denied");
  });

  it("can be created with an admin token and server returns an empty answer with the correct status code", async () => {
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(bookToBeCreated)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(201);

    expect(response.text.length).toBe(0);
  });

  it("cannot be created if id already exists and server returns an error message with the correct status code", async () => {
    const book = {
      id: 1,
      name: "Ohjelmointi C",
      author: "Kernighan & Ritchie",
      read: false
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(403);

    expect(JSON.parse(response.text).error).toEqual("id already exists, could not add the book");
  });
 
  it("cannot be created without 'name' field and server returns an error message with the correct status code", async () => {
    const book = {
      id: 2,
      author: "Franz Kafka",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'name' or 'author' fields were missing or empty strings");
  });

  it("cannot be created with empty 'name' field and server returns an error message with the correct status code", async () => {
    const book = {
      id: 2,
      name: "",
      author: "Franz Kafka",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'name' or 'author' fields were missing or empty strings");
  });

  it("cannot be created without 'author' field and server returns an error message with the correct status code", async () => {
    const book = {
      id: 2,
      name: "Oikeusjuttu",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'name' or 'author' fields were missing or empty strings");
  });

  it("cannot be created with empty 'author' field and server returns the correct status code", async () => {
    const book = {
      id: 2,
      name: "Oikeusjuttu",
      author: "",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'name' or 'author' fields were missing or empty strings");
  });

  it("cannot be created without 'id' field and server returns an error message with the correct status code", async () => {
    const book = {
      name: "Taide maailmassa",
      author: "Pentti Määttänen",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'id' was not a number or a number below 1");
  });

  it("cannot be created if 'id' is not a number and server returns an error message with the correct status code", async () => {
    const book = {
      id: "2",
      name: "Taide maailmassa",
      author: "Pentti Määttänen",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'id' was not a number or a number below 1");
  });

  it("cannot be created if 'id' is a number below 1 and server returns an error message with the correct status code - case zero", async () => {
    const book = {
      id: 0,
      name: "Taide maailmassa",
      author: "Pentti Määttänen",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'id' was not a number or a number below 1");
  });
  it("cannot be created if 'id' is a number below 1 and server returns an error message with the correct status code - case negative", async () => {
    const book = {
      id: -1,
      name: "Taide maailmassa",
      author: "Pentti Määttänen",
      read: true
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'id' was not a number or a number below 1");
  });

  it("cannot be created without 'read' field and server returns an error message with the correct status code", async () => {
    const book = {
      id: 2,
      name: "Taide maailmassa",
      author: "Pentti Määttänen"
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'read' field was not a boolean");
  });

  it("cannot be created if 'read' is not a boolean and server returns an error message with the correct status code", async () => {
    const book = {
      id: 2,
      name: "Taide maailmassa",
      author: "Pentti Määttänen",
      read: "true"
    };
    const response = await request(server)
      .post(baseUrlForBooks)
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'read' field was not a boolean");
  });

  // put requests
  it("cannot be edited without an admin token and server returns an error message with the correct status code", async () => {
    const book = {
      name: "Oikeusjuttu",
      author: "Fransz Kafka",
      read: false
    };
    const response = await request(server)
      .put(baseUrlForBooks + "/1")
      .send(book)
      .set("Authorization", createBearerStringForHeader(createTokenForUsername("testUser")))
      .expect(401);

    expect(JSON.parse(response.text).error).toEqual("access denied");
  });

  it("can be edited with an admin token and server returns an empty answer with the correct status code", async () => {

    const response = await request(server)
      .put(baseUrlForBooks + "/1")
      .send(bookToBeEdited)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(204);

    expect(response.text.length).toBe(0);
  });

  it("cannot be edited without 'name' field and server returns an error message with the correct status code", async () => {
    const book = {
      author: "Fransz Kafka",
      read: false
    };
    const response = await request(server)
      .put(baseUrlForBooks + "/1")
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'name' or 'author' fields were missing or empty strings");
  });

  it("cannot be edited with empty 'name' field and server returns an error message with the correct status code", async () => {
    const book = {
      name: "",
      author: "Fransz Kafka",
      read: false
    };
    const response = await request(server)
      .put(baseUrlForBooks + "/1")
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'name' or 'author' fields were missing or empty strings");
  });

  it("cannot be edited without 'author' field and server returns an error message with the correct status code", async () => {
    const book = {
      name: "Linna",
      read: true
    };
    const response = await request(server)
      .put(baseUrlForBooks + "/1")
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'name' or 'author' fields were missing or empty strings");
  });

  it("cannot be edited with empty 'author' field and server returns an error message with the correct status code", async () => {
    const book = {
      name: "Linna",
      author: "",
      read: false
    };
    const response = await request(server)
      .put(baseUrlForBooks + "/1")
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(400);

    expect(JSON.parse(response.text).error).toEqual("'name' or 'author' fields were missing or empty strings");
  });


  it("cannot be edited if book id does not exist and server returns an error message with the correct status code", async () => {
    const book = {
      name: "Linna",
      author: "Fransz Kafka",
      read: false
    };
    const response = await request(server)
      .put(baseUrlForBooks + "/15")
      .send(book)
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(404);

    expect(JSON.parse(response.text).error).toEqual("book not found");
  });

  // get:id requests
  it("can be fetched individually with a valid token and server returns a correct status code with an object", async () => {
    const response = await request(server)
      .get(baseUrlForBooks + "/1")
      .set("Authorization", createBearerStringForHeader(createTokenForUsername("testUser")))
      .expect(200);

    const bookReceived = JSON.parse(response.text);
    expect(bookReceived.name).toEqual(bookToBeEdited.name);
    expect(bookReceived.author).toEqual(bookToBeEdited.author);
    expect(bookReceived.read).toBe(bookToBeEdited.read);
  });

  it("cannot be fetched individually without a valid token and server returns an error message with the correct status code", async () => {
    const response = await request(server)
      .get(baseUrlForBooks + "/1")
      .expect(401);

    expect(JSON.parse(response.text).error).toEqual("token invalid");
  });

  it("cannot be fetched individually if book id does not exist and server returns an error message with the correct status code", async () => {
    const response = await request(server)
      .get(baseUrlForBooks + "/15")
      .set("Authorization", createBearerStringForHeader(createTokenForUsername("testUser")))
      .expect(404);

    expect(JSON.parse(response.text).error).toEqual("book not found");
  });

  // delete requests
  it("cannot be deleted without admin token and server returns an error message with the correct status code", async () => {
    const response = await request(server)
      .delete(baseUrlForBooks + "/1")
      .set("Authorization", createBearerStringForHeader(createTokenForUsername("testUser")))
      .expect(401);

    expect(JSON.parse(response.text).error).toEqual("access denied");
  });

  it("can be deleted with admin token and server returns an empty response with the correct status code", async () => {
    const response = await request(server)
      .delete(baseUrlForBooks + "/1")
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(204);

    expect(response.text.length).toBe(0);
  });

  it("cannot be deleted with a nonexisting book id and server returns an error message with the correct status code", async () => {
    const response = await request(server)
      .delete(baseUrlForBooks + "/15")
      .set("Authorization", createBearerStringForHeader(createAdminToken()))
      .expect(404);

    expect(JSON.parse(response.text).error).toEqual("book not found");
  });
});

it("trying to reach an invalid endpoint returns an error message with the correct status code", async () => {
  const response = await request(server)
    .get("/thisDoesNotExist")
    .expect(404);

  expect(JSON.parse(response.text).error).toEqual("unknown endpoint");
});