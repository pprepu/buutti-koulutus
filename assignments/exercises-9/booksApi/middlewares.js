import jwt from "jsonwebtoken";

export const bookValidator = (req, res, next) => {
  const { name, author, read } = req.body;

  const id = req.params.id 
    ? Number(req.params.id) 
    : req.body.id;

  if (isNaN(id) || typeof id !== "number" || id < 1) {
    return res.status(400).send({ error: "'id' was not a number or a number below 1" });
  }

  if (!name || !author) {
    return res.status(400).send({ error: "'name' or 'author' fields were missing or empty strings" });
  }

  if (typeof read !== "boolean") {
    return res.status(400).send({ error: "'read' field was not a boolean" });
  }

  next();
};

export const unknownEndpoint = (_req, res) => {
  res.status(404).send({ error: "unknown endpoint" });
};

export const logger = (req, _res, next) => {
  console.log(`${new Date().toUTCString()} --- ${req.method} ${req.url}`);
  if (Object.keys(req.body).length > 0) {
    console.log("   - body:", req.body);
  }
  console.log("---");
  next();
};

export const authenticate = (req, res, next) => {
  const auth = req.get("Authorization");
  if (!auth || !auth.startsWith("Bearer ")) {
    return res.status(401).send({ error: "token invalid"});
  }

  const token = auth.substring(7);

  try {
    const decodedToken = jwt.verify(token, process.env.SECRET);
    req.user = decodedToken;
    next();
  } catch (error) {
    return res.status(401).send({ error: "token invalid" });
  }
};

export const adminAuthenticate = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    res.status(401).send({ error: "access denied" });
  }
};