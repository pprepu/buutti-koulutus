POST http://localhost:3000/api/v1/books
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjY5MzIwNDkzfQ.DHCp7eeySU3gsbBFw6WYp_gkwpb9I_cAVonvpH3A8Ow

{
  "id": 0,
  "name": "The Human Condition",
  "author": "Hannah Arendt",
  "read": false
}