import express from "express";
import { bookValidator, adminAuthenticate } from "./middlewares.js";
const router = express.Router();
let books = [];

router.get("/", (_req, res) => {
  res.status(200).send(books);
});

router.get("/:id", (req, res) => {
  const id = Number(req.params.id);
  const book = books.find(book => book.id === id);
  if (!book) {
    return res.status(404).send({ error: "book not found" });
  }
  res.status(200).send(book);
});

router.delete("/:id", adminAuthenticate, (req, res) => {
  const id = Number(req.params.id);
  const booksFiltered = books.filter(book => book.id !== id);
  if (books.length === booksFiltered.length) {
    return res.status(404).send({ error: "book not found" });
  }
  books = booksFiltered;
  res.status(204).send();
});

router.post("/", adminAuthenticate, bookValidator, (req, res) => {
  const bookReceived = req.body;
  if (books.some(book => book.id === bookReceived.id)) {
    return res.status(403).send({ error: "id already exists, could not add the book" });
  }
  books = books.concat(bookReceived);
  res.status(201).send();
});

router.put("/:id", adminAuthenticate, bookValidator, (req, res) => {
  const id = Number(req.params.id);
  const bookUpdated = req.body;
  if (!books.find(book => book.id === id)) {
    return res.status(404).send({ error: "book not found" });
  }
  // this version doesn't change book ids or add new fields
  books = books.map(book => book.id === id 
    ? { ...book, name: bookUpdated.name, author: bookUpdated.author, read: bookUpdated.read } 
    : book
  );
  res.status(204).send();
});

export default router;