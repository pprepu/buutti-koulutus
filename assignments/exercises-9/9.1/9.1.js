import http from "http";
const PORT = 5000;

const server = http.createServer((_req, res) => {
  res.write("Hello world!");
  res.end();
});

server.listen(PORT);

console.log(`listening to ${PORT}`);