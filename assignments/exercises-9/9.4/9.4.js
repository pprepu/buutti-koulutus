import express from "express";
const PORT = 3000;

const server = express();

let counter = {};

server.get("/counter/:name", (req, res) => {
  const name = req.params.name;

  if (!counter[name]) {
    counter[name] = 0;
  }

  res.send(`<h1> ${name} was here ${++counter[name]} time${counter[name] === 1 ? "" : "s"}</h1>`);
});

server.listen(PORT, () => {
  console.log("listening to " + PORT);
});