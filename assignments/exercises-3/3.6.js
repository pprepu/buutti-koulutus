const arr1 = [1, 2, 3, 3, 4];
const arr2 = [4, 4, 5, 6, 6, 6];

// spread syntax + reduce
console.log([...arr1, ...arr2].reduce((acc, cur) => !acc.includes(cur) ? [...acc, cur] : acc, []));

// extra: concat + filter + short-circuiting
console.log(arr1
  .concat(arr2)
  .sort()
  .filter((number, index, array) => index === 0 || array[index - 1] !== number)
);