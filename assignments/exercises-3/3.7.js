const generateRandomNumber = (min, max) => min + Math.floor(Math.random() * ((max + 1) - min));

const generateLotteryNumbers = () => {
  let lotteryNumbers = [];

  while (lotteryNumbers.length < 7) {
    let randomNumber = generateRandomNumber(1, 40);
    if (!lotteryNumbers.includes(randomNumber)) {
      lotteryNumbers = [...lotteryNumbers, randomNumber];
    }
  }

  return lotteryNumbers;
};

const lotteryNumbers = generateLotteryNumbers();
console.log(lotteryNumbers);

// extra
const printElementSlowly = (printedElement, index) => setTimeout(() => console.log(printedElement), index * 1000);

lotteryNumbers.forEach(printElementSlowly);