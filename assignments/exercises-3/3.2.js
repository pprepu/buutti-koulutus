/**
 * A function that generates a random number between two values
 * @param {number} min smallest possible random number
 * @param {number} max highest possible random number
 * @returns randomized number between min and max (both included)
 */

// using Math.floor, because Math.round would lead to under-representation of edge cases 
// (why? less ways to get rounded into those compared to middle values)
// also, +1 added to the range(|max-min|), because both min and max are included
const generateRandomNumber = (min, max) => min + Math.floor(Math.random() * (max - min + 1));

let generatedNumbers = [];
const testCases = 200;
const min = 2;
const max = 8;
for (let i = 0; i < testCases; i++) {
  // mutable version - immutable version possible w/ concat (for example)
  generatedNumbers.push(generateRandomNumber(min, max));
}

console.log(generatedNumbers);

// impromptu testing (without jest)...
const numbersOutOfRange = generatedNumbers.filter(number => number > max || number < min);
if (numbersOutOfRange.length !== 0) {
  throw new Error("Number(s) created out of wanted range!");
}

// some stats because why not?
const generatedNumberFrequencies = {};

generatedNumbers.forEach(number => generatedNumberFrequencies[number] 
  ? generatedNumberFrequencies[number] = generatedNumberFrequencies[number] + 1 
  : generatedNumberFrequencies[number] = 1
);

const printObjectKeysAndValues = object => {
  for (let key in object) {
    console.log(`${key}: ${object[key]}`);
  }
};

console.log("frequencies of different numbers:");
printObjectKeysAndValues(generatedNumberFrequencies);