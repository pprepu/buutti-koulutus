const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

// not handling more than 20 competitors correctly
const mapCompetitors = competitors => competitors
  .map((competitor, index) => index < ordinals.length 
    ? `${(index + 1) + ordinals[index]} competitor was ${competitor}`
    : `${(index + 1) + ordinals[ordinals.length - 1]} competitor was ${competitor}`
  );
// v2 with helper function - should handle any number of competitors
const getOrdinalFromArrayIndex = index => {
  let placementAsString = (index + 1).toString();
  let ordinalIndex = placementAsString[placementAsString.length - 1] - 1;
  if (
    placementAsString.length > 1 && placementAsString[placementAsString.length -2] === "1"
    || ordinalIndex >= ordinals.length
    || placementAsString[placementAsString.length - 1] === "0"
  ) {
    ordinalIndex = ordinals.length - 1;
  }
  return placementAsString + ordinals[ordinalIndex];
};

const mapCompetitors2 = competitors => competitors.map((competitor, index) => `${getOrdinalFromArrayIndex(index)} competitor was ${competitor}`);

console.log("v1", mapCompetitors(competitors));

const createBiggerArrayWithClonedElements = (array, sizeMultiplier) => {
  let clonedArray = [...array];
  for (let i = 0; i < sizeMultiplier; i++) {
    clonedArray = clonedArray.concat(array);
  }
  return clonedArray;
};

console.log("v2");
console.log(mapCompetitors2(competitors.slice(4)));
console.dir(mapCompetitors2(createBiggerArrayWithClonedElements(competitors, 50)), {"maxArrayLength": null});