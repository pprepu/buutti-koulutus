// onko 0 validi runway number?? - tämä funktio voi palauttaa nollan...
const findRunwayNumber = degrees => {

  const roundingToTen = degrees % 10;
  degrees -= roundingToTen;
  if (roundingToTen >= 5) {
    degrees += 10;
  }

  return degrees / 10;

};

for (let i = 0; i < 10; i++) {
  const someNumber = i * 17.5;
  console.log(someNumber, findRunwayNumber(someNumber));
}