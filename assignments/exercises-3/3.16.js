export const isPalindrome = string => string.toLowerCase() === string.split("").reverse().join("").toLowerCase();

const [, , string] = process.argv;

if (string) {
  isPalindrome(string)
    ? console.log(`Yes, '${string}' is a palindrome`)
    : console.log(`No, '${string}' is not a palindrome`);
}