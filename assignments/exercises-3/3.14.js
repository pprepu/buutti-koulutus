const createArrayFromRange = (start, end) => {
  const arr = [];

  if (start > end) {
    for (let i = start; i >= end; i--) {
      arr.push(i);
    }
  } else {
    for (let i = start; i <= end; i++) {
      arr.push(i);
    }
  }
  return arr;
};

// same logic, shorter but less readable, immutable array operations
const createArrayFromRange2 = (start, end) => {
  let arr = [];

  for (let i = start; start > end ? i >= end : i <= end; start > end ? i-- : i++) {
    arr = arr.concat(i);
  }
  return arr;
};

// then something.. interesting?
const createArrayFromRangeChaoticEvil = (start, end) => 
  [...Array(Math.abs(start - end) + 1).keys()]
    .map(number => start < end ? number + start : start - number);

let [start, end] = process.argv.slice(2);
start = Number(start);
end = Number(end);

console.log("v1", createArrayFromRange(start, end));
console.log("v2", createArrayFromRange2(start, end));
console.log("v3", createArrayFromRangeChaoticEvil(start, end));