const kertoma = n => {
  if (n === 0) {
    return 1;
  } else if (n === 1) {
    return n;
  } else if (n > 1) {
    return n * kertoma(n - 1);
  } else {
    return NaN;
  }
};

console.log(kertoma(4));
console.log(kertoma(10));
console.log(kertoma(0));
console.log(kertoma("what"));