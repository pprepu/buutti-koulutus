const fibonacci = n => {
  if (n < 1 || isNaN(n)) {
    return null;
  }

  if (n === 1) {
    return [0];
  }
  const fiboArray = [0, 1];

  for (let i = 2; i < n; i++) {
    fiboArray.push(fiboArray[i-2] + fiboArray[i-1]);
  }

  return fiboArray;
};

console.log(fibonacci(8));
console.log(fibonacci(12));
console.log(fibonacci(2));


// v2 with recursion
const findNthFibonacciNumber = nthNumberInSequence => {
  if (nthNumberInSequence === 1) {
    return 0;
  }

  if (nthNumberInSequence === 2) {
    return 1;
  }

  return findNthFibonacciNumber(nthNumberInSequence - 2) + findNthFibonacciNumber(nthNumberInSequence - 1);
};

const fibonacciWithRecursiveHelperFunction = n => {
  const fiboArray = [];
  for (let i = 1 ; i <= n; i++) {
    fiboArray.push(findNthFibonacciNumber(i));
  }
  return fiboArray;
};

console.log("v2, recursive");
console.log(fibonacciWithRecursiveHelperFunction(8));
console.log(fibonacciWithRecursiveHelperFunction(12));
console.log(fibonacciWithRecursiveHelperFunction(2));