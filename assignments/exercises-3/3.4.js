const arr = [];

for (let i = 1; i < 23; i++) {
  arr.push(i);
}

console.log("original array:", arr);

// 1
console.log("divisible by 3:", arr.filter(number => number % 3 === 0));

// 2
console.log("number * 2:", arr.map(number => number * 2));

// 3
console.log("sum:", arr.reduce((accumulator, currentValue) => accumulator + currentValue, 0));