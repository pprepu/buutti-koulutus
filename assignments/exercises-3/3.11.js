const countSheep = sheep => {
  let sheepArray = [];
  for (let i = 1; i <= sheep; i++) {
    sheepArray = sheepArray.concat(`${i} sheep`);
  }

  return sheepArray.join("...") + "...";
};

const args = process.argv.slice(2);
console.log(countSheep(args[0]));