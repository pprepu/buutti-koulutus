const toUpperCase = (string, debugging = false) => {
  const words = string.split(" ");
  if (debugging) {
    console.log(string, words);
  }
  return words.map(word => word ? word[0].toUpperCase() + word.substring(1) : word).join(" ");
};

// version 2 with a couple differences
// 1. oneliner...
// 2. using && instead of ? to prevent calling methods on possible undefined values (word[0] on an empty string)
const toUpperCase2 = string => string.split(" ").map(word => word && word[0].toUpperCase() + word.substring(1)).join(" ");

console.log(toUpperCase("testing a random string"));
console.log(toUpperCase("  tes ting   a    random   string"));

console.log(toUpperCase("t a r"));
console.log(toUpperCase("  t  a r   "));
// console.log(toUpperCase("  t  a r   ", true));

console.log("---v2---");

console.log(toUpperCase2("testing a random string"));
console.log(toUpperCase("t a r"));
console.log(toUpperCase2("  t  a r   "));