const reverseWords = string => string
  .split(" ")
  .map(word => word
    .split("")
    .reverse()
    .join("")
  )
  .join(" ");

const [, , string] = process.argv;

if (string) {
  console.log(reverseWords(string));
}