const isPrime = number => {
  if (number < 2) {
    return false;
  }
  if (number === 2) {
    return true;
  }

  for (let i = 2; i < number; i++) {
    if (number % i === 0) {
      return false;
    }
  }

  return true;
};

for (let i = 1; i <= 30; i++) {
  console.log(`${i} is a prime: ${isPrime(i)}`);
}