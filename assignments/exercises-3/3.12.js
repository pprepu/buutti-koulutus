const createCharIndexObject = () => {
  const charIndexObj = {};
  for (let charIndex = 0; charIndex < 26; charIndex++) {
    charIndexObj[String.fromCharCode(97 + charIndex)] = charIndex + 1;
  }

  return charIndexObj;
};
const charIndex = createCharIndexObject();
// dependency injection?
const transformCharsToIndex = (string, charIndex) => string.replace(/\w/g, char => charIndex[char]);

if (process.argv[2]) {
  console.log(transformCharsToIndex(process.argv[2], charIndex));
}