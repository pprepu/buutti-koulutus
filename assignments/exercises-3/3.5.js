// first idea, loops inside a loop, less efficient (although who cares?)
const nonRepeatingLetter = string => {
  for (let letter of string) {
    if (string.indexOf(letter) === string.lastIndexOf(letter)) {
      return letter;
    }
  }
  return null;
};

// one-liner version for fun(?)
const nonRepeatingLetterOneLiner = string => string.split("").find(letter => string.indexOf(letter) === string.lastIndexOf(letter));

// same letters are grouped together, which means that if a letter is surrounded by different letters, 
// -> it must be a non-repeating one
// solution is more efficient, but seems unnecessarily verbose
const nonRepeatingLetter2 = string => {
  if (string.length === 0) {
    return;
  }

  if (string.length === 1) {
    return string[0];
  }
  // checking first and last letters separately - they only need to be compared to one of their neighbours
  if (string[0] !== string[1]) {
    return string[0];
  }

  for (let letterIndex = 1; letterIndex < string.length - 1; letterIndex++) {
    if (string[letterIndex - 1] !== string[letterIndex] && string[letterIndex + 1] !== string[letterIndex]) {
      return string[letterIndex];
    }
  }

  if (string[string.length - 1] !== string[string.length - 2]) {
    return string[string.length - 1];
  }

  return null;

};

// using .find to find the first instances of each letter, then applying same logic with version 2
const nonRepeatingLetter3 = string => string
  .split("")
  .find((letter, index, arr) => letter !== arr[index - 1] && letter !== arr[index + 1]);

// let testString = "aabboooooffffkkccjdddTTT";
let testString = "aaAAbboooooffffkkccjjdT";
console.log("v1", testString, nonRepeatingLetter(testString));
console.log("v1-oneliner:", testString, nonRepeatingLetterOneLiner(testString));
console.log("v2:", testString, nonRepeatingLetter2(testString));
console.log("v3:", testString, nonRepeatingLetter3(testString));