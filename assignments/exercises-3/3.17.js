const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

const selectRandomIndexFromArray = array => Math.floor(Math.random() * array.length);

const createNewRandomizedOrderFromArray = array => {
  const copyOfOriginalArray = [...array];
  let newOrder = [];
  
  while (copyOfOriginalArray.length > 0) {
    newOrder = newOrder.concat(copyOfOriginalArray.splice(selectRandomIndexFromArray(copyOfOriginalArray), 1));
  }

  return newOrder;
};

console.log(createNewRandomizedOrderFromArray(array));