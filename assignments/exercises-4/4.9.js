const testStrings = ["kkkggllrrccmmnnöä", "jessssss", "talo", "koira", "hunajaa", "abracadabra", "aeiouy", "aax eex uux y"];

// string-to-array approach
const getVowelCount = string => string
  .split("")
  .filter(char => "aeiouyAEIOUY".includes(char))
  .length;

// regex approach
const getVowelCountRegex = string => string
  .replace(/[^aeiouy]/gi, "")
  .length;

for (const string of testStrings) {
  console.log("v1:", string, getVowelCount(string));
  console.log("v2:", string, getVowelCountRegex(string));
}