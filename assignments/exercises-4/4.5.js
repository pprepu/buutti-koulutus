import fs from "fs";

const fileName = "forecast_data";

try {
  const data = fs.readFileSync(`${fileName}.json`, "utf-8");
  const forecast = JSON.parse(data);
  const modifiedForecastAsJsonString = JSON.stringify({...forecast, temperature: 8}, null, 4);
  fs.writeFileSync("forecast_data.json", modifiedForecastAsJsonString, "utf-8");
} catch (error) {
  console.error("An error happened while dealing with the file", error);
}