const average = array => array.reduce((acc, cur) => acc + cur, 0) / array.length;
const aboveAverage = array => array.filter(number => number > average(array));

// saving 'average' to a variable, so it doesn't need to be re-calculated on every element
const aboveAverage2 = array => {
  const averageNumber = average(array);
  return array.filter(number => number > averageNumber);
};

console.log(aboveAverage([1, 5, 9, 3]));
console.log(aboveAverage2([1, 5, 9, 3]));