const generateRandomNumberInRange = (min, max) => min + Math.floor(Math.random() * (max - min + 1));

const currentYearString = new Date().getFullYear().toString();

const generateUsername = (firstname, lastname) => 
  "B" +
  currentYearString.substring(currentYearString.length - 2) +
  lastname.substring(0, 2).toLowerCase() +
  firstname.substring(0, 2).toLowerCase();

const generatePassword = (firstname, lastname) => 
  String.fromCharCode(generateRandomNumberInRange(65, 90)) +
  firstname[0].toLowerCase() +
  lastname[lastname.length - 1].toUpperCase() +
  String.fromCharCode(generateRandomNumberInRange(33, 47)) +
  currentYearString.substring(currentYearString.length - 2); 

const generateCredentials = (firstname, lastname) => {
  return [
    generateUsername(firstname, lastname), 
    generatePassword(firstname, lastname)
  ];
};

console.log("John Doe ->", generateCredentials("John", "Doe"));

// optional command line arguments:
const [firstname, lastname] = process.argv.slice(2);
firstname && lastname && console.log(`${firstname} ${lastname} ->`, generateCredentials(firstname, lastname));