// logging stuff to a file (4.12.json) for memes...
import fileService from "../services/files.js";

// v1 - recursive approach
const recursiveCollatzCounter = n => {
  if (n === 1) {
    return 0;
  }

  if (n % 2 === 0) {
    return recursiveCollatzCounter(n / 2) + 1;
  }

  return recursiveCollatzCounter(n * 3 + 1) + 1;
};

// v1 refactored with ? operator
const recursiveCollatzCounterRefactor = n => {
  return n === 1
    ? 0
    : n % 2 === 0
      ? recursiveCollatzCounter(n / 2) + 1
      : recursiveCollatzCounter(n * 3 + 1) + 1;
};

// v2 - log the whole sequence, iterative approach
const collatzLogger = n => {
  const log = [];
  while (n !== 1) {
    log.push(n);
    if (n % 2 === 0) {
      n /= 2;
    } else {
      n = n * 3 + 1;
    }
  }
  log.push(n);
  return log;
};

// v3 - logger with recursive approach
const recursiveCollatzLogger = n => {
  if (n === 1) {
    return [n];
  }

  if (n % 2 === 0) {
    return [n].concat(recursiveCollatzLogger(n / 2));
  }

  return [n].concat(recursiveCollatzLogger(n * 3 + 1));
};


// helper for both v2 and v3
const collatzLogToString = collatzLog => {
  return `It took ${collatzLog.length - 1} step${collatzLog.length - 1 !== 1 ? "s" : ""} to get from ${n} to 1\nLog: ${collatzLog.join(" => ")}`;
};

// handling command line arguments
const n = Number(process.argv[2]);

if (n > 0) {
  // loading data from 4.12.json
  const dataObject = fileService.loadAndParseJson();
  // logging
  console.log("counter1:", recursiveCollatzCounter(n), "counter2:", recursiveCollatzCounterRefactor(n));
  console.log("--- v2");
  console.log(collatzLogToString(collatzLogger(n)));
  console.log("--- v3");
  console.log(collatzLogToString(recursiveCollatzLogger(n)));
  if (dataObject?.timesRun > 20) {
    console.log("   Maybe it's time to go outside and meet some friends?\n   xkcd-link:", dataObject.link);
  }
  // saving updated data to 4.12.json
  dataObject && fileService.stringifyObjectAndSaveAsJson({...dataObject, timesRun: ++dataObject.timesRun});
} else {
  console.error("ERROR: Provide one number argument, which is larger than 0");
}