const Ingredient = function(name, amount) {
  this.name = name;
  this.amount = amount;
};

Ingredient.prototype.toString = function() {
  return `${this.name}, amount: ${this.amount}`;
};

Ingredient.prototype.scale = function(factor) {
  this.amount *= factor;
};

const Recipe = function(name, servings, ingredients = []) {
  this.name = name;
  this.servings = servings;
  this.ingredients = ingredients;
};

Recipe.prototype.toString = function() {
  let str = `\n${this.name}: ${this.servings} servings\n\n`;
  str += this.ingredients.map(ingredient => "  " + ingredient.toString()).join("\n");
  return str;
};

Recipe.prototype.setServings = function(servings) {
  const scale = servings / this.servings;
  this.ingredients.forEach(ingredient => ingredient.scale(scale));
  this.servings = servings;
};

class HotRecipe extends Recipe {
  constructor(heat, name, servings, ingredients) {
    super(name, servings, ingredients);
    this.heatLevel = heat;
  }
  toString() {
    return this.heatLevel > 5 
      ? `${super.toString()}\nWARNING! This recipe might be quite hot (heat: ${this.heatLevel})`
      : super.toString();
  }
}
const sipuli = new Ingredient("sipuli", 1);
const punasipuli = new Ingredient("punasipuli", 1);
const tulisetReseptit = [
  new HotRecipe(3, "Sipulilautanen chilillä", 1, [sipuli, punasipuli, new Ingredient("chili", 1)]),
  new HotRecipe(8, "Chililautanen sipulilla", 1, [new Ingredient("sipuli", 1), new Ingredient("chili", 8)])
];

tulisetReseptit.forEach(recipe => {
  recipe.setServings(100);
  console.log(recipe.toString());
});