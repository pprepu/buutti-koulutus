const likes = array => {
  switch(array.length) {
  case 0:
    return "no one likes this";
  case 1:
    return `${array[0]} likes this`;
  case 2:
    return `${array.join(" and ")} like this`;
  case 3:
    return `${array[0]}, ${array.slice(1).join(" and ")} like this`;
  default:
    return `${array.slice(0, 2).join(", ")} and ${array.length - 2} others like this`;
  }
};

const names = ["Alex", "Linda", "Mark", "Max", "Teppo", "Andy", "Lisa"];

console.log(likes(
  names.reduce((acc, cur) => {
    console.log(likes(acc));
    return acc.concat(cur);
  }, [])
));