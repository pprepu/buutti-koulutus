const scoreAnswer = (correctAnswer, answer) => {
  if (!answer) {
    return 0;
  }

  return answer === correctAnswer ? 4 : -1;
};

const checkExam = (correctAnswers, studentAnswers) => {
  const totalScore = correctAnswers
    .reduce((totalScore, currentCorrectA, i) => totalScore + scoreAnswer(currentCorrectA, studentAnswers[i]), 0);
  
  if (totalScore < 0) {
    return 0;
  }

  return totalScore;
};

// version 2: using more array methods + palindrome check from assignment 3.16
// since importing was a topic last time, I thought it would be funny to come up with an idea that uses some something I've done previously
// in other words, I'm basically just memeing here
// efficiency isn't optimized either (going through the list multiple times), but at least input sizes are known and small
// although all steps could be combined to a single reduce, I wanted to find out if keeping all separate steps in would be more readable
import { isPalindrome } from "../exercises-3/3.16.js";
// mapping correct + student answers together to a combined string
// -> filtering elements out that don't affect scoring (student hasn't answered => length of string < 2)
// --> score rest of the elements based on if they are palindromes and combine these scores to a sum
const checkExam2 = (correctAnswers, studentAnswers) => {
  const totalScore = correctAnswers
    .map((answerCorrect, i) => answerCorrect + studentAnswers[i])
    .filter(answersCombined => answersCombined.length > 1)
    .map(answersCombinedAndFiltered => isPalindrome(answersCombinedAndFiltered) ? 4 : -1)
    .reduce((acc, cur) => acc + cur, 0);

  return totalScore < 0 ? 0 : totalScore;
};

console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"]));
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b",  ""]));
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"]));
console.log(checkExam(["b", "c", "b", "a"], ["",  "a", "a", "c"]));
console.log("---");
console.log(checkExam2(["a", "a", "b", "b"], ["a", "c", "b", "d"]));
console.log(checkExam2(["a", "a", "c", "b"], ["a", "a", "b",  ""]));
console.log(checkExam2(["a", "a", "b", "c"], ["a", "a", "b", "c"]));
console.log(checkExam2(["b", "c", "b", "a"], ["",  "a", "a", "c"]));