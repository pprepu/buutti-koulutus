const Ingredient = function(name, amount) {
  this.name = name;
  this.amount = amount;
};

Ingredient.prototype.toString = function() {
  return `${this.name}, amount: ${this.amount}`;
};

Ingredient.prototype.scale = function(factor) {
  this.amount *= factor;
};

const Recipe = function(name, servings, ingredients = []) {
  this.name = name;
  this.servings = servings;
  this.ingredients = ingredients;
};

Recipe.prototype.toString = function() {
  let str = `\n${this.name}: ${this.servings} servings\n\n`;
  str += this.ingredients.map(ingredient => "  " + ingredient.toString()).join("\n");
  return str;
};

Recipe.prototype.setServings = function(servings) {
  const scale = servings / this.servings;
  this.ingredients.forEach(ingredient => ingredient.scale(scale));
  this.servings = servings;
};

const jauho = new Ingredient("puolikarkea vehnäjauho", 300);
const suola = new Ingredient("suola", 4,5);
const hiiva = new Ingredient("hiiva", 3);
const vesi = new Ingredient("vesi", 210);

const leipa = new Recipe("no-knead leipä", 4, [jauho, suola, hiiva, vesi]);

console.log(leipa.toString());
leipa.setServings(100);
console.log(leipa.toString());