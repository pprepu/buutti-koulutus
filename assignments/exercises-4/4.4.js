import fs from "fs";
const myFile = "./myText.txt";
const newFile = "./newText.txt";

const writeStream = fs.createWriteStream(newFile);
const readStream = fs.createReadStream(myFile, "utf-8");
readStream.on("data", txt => {
  let stringFromFileEdited = txt
    .replaceAll("joulu", "kinkku")
    .replaceAll("lapsilla", "poroilla")
    .replaceAll("Joulu", "Kinkku")
    .replaceAll("Lapsilla", "Poroilla");

  writeStream.write(stringFromFileEdited, (err) => {
    if (err) console.error(err);
  });
});