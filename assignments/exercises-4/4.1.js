const Ingredient = function(name, amount) {
  this.name = name;
  this.amount = amount;
  this.toString = function() {
    return `${this.name}, amount: ${this.amount}`;
  };
};

const sipuli = new Ingredient("sipuli", 2);
const punasipuli = new Ingredient("punasipuli", 1);

console.log(sipuli);
console.log(sipuli.toString());
console.log(punasipuli.toString());

const Recipe = function(name, servings, ingredients = []) {
  this.name = name;
  this.servings = servings;
  this.ingredients = ingredients;
  this.toString = function() {
    let str = `\n${this.name}: ${this.servings} servings\n\n`;
    str += this.ingredients.map(ingredient => "  " + ingredient.toString()).join("\n");
    return str;
  };
};

const sipuliAnnos = new Recipe("Sipulilautanen", 2, [sipuli, punasipuli]);

console.log(sipuliAnnos.toString());