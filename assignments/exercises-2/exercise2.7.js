const fizzBuzz = () => {
  let tulostettava = "";

  for (let i = 1; i <= 100; i++) {
    tulostettava = "";
    if (i % 3 === 0) {
      tulostettava += "Fizz";
    }

    if (i % 5 === 0) {
      tulostettava += "Buzz";
    }

    if (tulostettava) {
      console.log(tulostettava);
    } else {
      console.log(i);
    }
  }
};

fizzBuzz();