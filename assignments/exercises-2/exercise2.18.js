const args = process.argv.slice(2, 5);

if (args.length < 3 || args.includes("")) {
  console.log("3 names have to be provided, no empty strings allowed");
} else {
  console.log("initial letters:", args.map(name => name[0].toLowerCase()). join("."));
  args.sort((name1, name2) => name2.length - name1.length);
  console.log("length comparison:", args.join(" "));
}