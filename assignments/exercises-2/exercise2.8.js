// level 1
const generateRow = n => {
  let row = "";
  for (let i = 0; i < n; i++) {
    row += "&";
  }

  return row;
};

const triangle = n => {
  for (let i = 1; i <= n; i++) {
    console.log(generateRow(i));
  }
};

console.log("level 1:");
triangle(4);

// level 2
const generateRow2 = (size, n) => {
  let row = "";
  let emptySpaces = size - n;
  let triangleSize = 1 + (n - 1) * 2;

  for (let i = 0; i < emptySpaces; i++) {
    row += " ";
  }

  for (let i = 0; i < triangleSize; i++) {
    row += "&";
  }

  return row;
};

const triangle2 = n => {
  for (let i = 1; i <= n; i++) {
    console.log(generateRow2(n, i));
  }
};
console.log("level 2:");
triangle2(4);
triangle2(7);

// level 3

const generateRow3 = (size, n) => {
  let row = "";
  let emptySpaces = size - n;
  let triangleSize = 1 + (n - 1) * 2;

  let currentCharIndex = 0;

  while (currentCharIndex < emptySpaces + triangleSize) {
    currentCharIndex < emptySpaces
      ? row += " "
      : row += "&";

    currentCharIndex++;
  }
  return row;
};

const triangle3 = n => {
  let currentRow = 1;
  while (currentRow <= n) {
    console.log(generateRow3(n, currentRow));
    currentRow++;
  }
};

console.log("level 3:");
triangle3(4);