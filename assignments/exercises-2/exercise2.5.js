const sumOfInts = n => {
  let sum = 0;
  for (let i = 1; i <= n; i++) {
    sum += i;
  }
  return sum;
};

const sumOfInts2 = n => {
  let sum = 0;
  let currentNumber = 1;

  while (currentNumber <= n) {
    sum += currentNumber;
    currentNumber++;
  }

  return sum;
};

console.log(sumOfInts(5));
console.log("---");
console.log(sumOfInts2(5));