const args = process.argv.slice(2, 5);

if (args.length === 3) {
  console.log("numbers given:", args.join(", "));
  console.log("largest:", Math.max(...args));
  console.log("smallest:", Math.min(...args));

  // alla oleva koodi ei ole mielestäni kovin selkeetä, mutta tein sen lähinnä omaksi ilokseni
  // ehkä selkein tapa ottaen huomioon muu tehtävänanto, olisi tsekkaa onko pienin numero === suurin numero tms.
  args.every(number => number === args[0])
    && console.log("all values are equal");
} else {
  console.log("try giving (at least) 3 numbers!");
}