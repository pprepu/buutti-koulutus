const args = process.argv.slice(2, 4);

if (args.length < 2) {
  console.log("2 arguments are needed: 1. lower/upper 2. the string itself");
} else if (args[0] !== "lower" && args[0] !== "upper") {
  console.log("1st argument has to be 'upper' or 'lower'");
} else {
  const modifiedString = args[0] === "lower"
    ? args[1].toLowerCase()
    : args[1].toUpperCase();
  console.log(modifiedString);
}