const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

// versio 1 - reduce
console.log("v1:", arr.reduce((prev, cur) => cur > prev ? cur : prev));

// versio 2
let largest = -Infinity;
for (let i = 0; i < arr.length; i++) {
  if (arr[i] > largest) {
    largest = arr[i];
  }
}

console.log("v2:", largest);

// versio 3 - for of
let largestNum = -Infinity;
for (const number of arr) {
  if (number > largestNum) {
    largestNum = number;
  }
}

console.log("v3:", largestNum);

// extra
// v1
console.log("extra v1:");
const highestNumber = arr.reduce((prev, cur) => cur > prev ? cur : prev);
// jos taulukossa on monta yhtä suurta suurinta numeroa, palautetaan näiden jälkeen suurin alkio
console.log("2nd highest:", arr.reduce((prev, cur) => cur !== highestNumber && cur > prev ? cur: prev));

// v2
let largestNumber = -Infinity;
let secondLargestNumber = -Infinity;
for (const number of arr) {
  if (number > largestNumber) {
    secondLargestNumber = largestNumber;
    largestNumber = number;
  } else if (number > secondLargestNumber) {
    secondLargestNumber = number;
  }
}

console.log("extra v2:");
console.log("2nd highest:", secondLargestNumber);