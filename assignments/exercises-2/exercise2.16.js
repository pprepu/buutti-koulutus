const monthReceived = Math.floor(process.argv[2]);
const daysInMonths = {
  1: ["31", "January"],
  2: ["28 or 29", "February"],
  3: ["31", "March"],
  4: ["30", "April"],
  5: ["31", "May"],
  6: ["30", "June"],
  7: ["31", "July"],
  8: ["31", "August"],
  9: ["30", "September"],
  10: ["31", "October"],
  11: ["30", "November"],
  12: ["31", "December"],
};
if (isNaN(monthReceived) || monthReceived < 1 || monthReceived > 12) {
  console.log("argument must be a number between 1 and 12");
} else {
  console.log(`There are ${daysInMonths[monthReceived][0]} days in ${daysInMonths[monthReceived][1]}.`);
}