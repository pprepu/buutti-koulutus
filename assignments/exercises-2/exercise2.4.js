// 1
for (let i = 0; i <= 1000; i += 100) {
  console.log(i);
}

// 2
for (let i = 1; i <= 128; i *= 2) {
  console.log(i);
}

// 3
for (let i = 3; i <= 15; i += 3) {
  console.log(i);
}

// 4
for (let i = 9; i >= 0; i--) {
  console.log(i);
}

// 5
for (let i = 1; i <= 4; i++) {
  for (let y = 0; y < 3; y++) {
    console.log(i);
  }
}

// for (let i = 1; i < 5; i += 0.34) {
//   console.log(Math.floor(number));
// }

// 6
for (let i = 0; i < 3; i++) {
  for (let y = 0; y < 5; y++) {
    console.log(y);
  }
}