const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];

console.log(arr[2], arr[4], arr.length);

arr.sort();
console.log(arr);

arr.push("sipuli");
console.log(arr);

// extra
arr.shift();
console.log(arr);

arr.forEach(alkio => console.log(alkio));
console.log("---");
arr
  .filter(alkio => alkio.includes("r"))
  .forEach(alkio => console.log(alkio));