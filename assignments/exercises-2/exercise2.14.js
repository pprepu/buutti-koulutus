const args = process.argv.slice(2);

const numA = Number(args[0]);
const numB = Number(args[1]);

if (isNaN(numA) || isNaN(numB)) {
  // mm. tyhjän stringin käyttö argumenttina onnistuu, koska kääntyy nollaksi
  console.log("arguments have to be convertable to numbers!");
} else {
  if (numA > numB) {
    console.log("A is greater");
  } else if (numA < numB) {
    console.log("B is greater");
  } else if (args[2] === "hello world" && numA === numB) {
    console.log("yay, you guessed the password");
  } else if (numA === numB) {
    console.log("A and B are equal");
  } else {
    console.log("something unexpected has happened.");
  }
}