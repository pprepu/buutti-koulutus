const students = [
  { name: "Markku", score: 99 },
  { name: "Karoliina", score: 58 },
  { name: "Susanna", score: 69 },
  { name: "Benjamin", score: 77 },
  { name: "Isak", score: 49 },
  { name: "Liisa", score: 89 },
];

// 1
const highestScoreStudent = students.reduce((prev, cur) => cur.score > prev.score ? cur : prev);
const lowestScoreStudent = students.reduce((prev, cur) => cur.score < prev.score ? cur : prev);

console.log("1 - v1:", highestScoreStudent, lowestScoreStudent);

// yhden loopin versio
let highest = students[0];
let lowest = students[0];

for (const student of students) {
  if (student.score > highest.score) {
    highest = student;
    continue;
  }

  if (student.score < lowest.score) {
    lowest = student;
  }
}

console.log("1 - v2:", highest, lowest);

// 2

const averageScore = students
  .map(student => student.score)
  .reduce((sum, score) => sum += score)
  / students.length;

console.log("2:", averageScore);

// 3

console.log("3:");
students
  .filter(student => student.score > averageScore)
  .forEach(student => console.log(student));

// console.log(students.filter(student => student.score > averageScore));

// 4

const getGrade = grade => {
  if (grade < 40) {
    return 1;
  }
  if (grade < 60) {
    return 2;
  }
  if (grade < 80) {
    return 3;
  }
  if (grade < 95) {
    return 4;
  }

  return 5;
};

for (const student of students) {
  student["grade"] = getGrade(student.score);
}

console.log("4:", students);