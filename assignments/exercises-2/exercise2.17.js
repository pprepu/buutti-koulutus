const checkBalance = true;
const isActive = true;
const balance = 1_000;

if (checkBalance) {
  if (balance > 0 && isActive) {
    console.log("Your balance:", balance);
  } else {
    if (!isActive) {
      console.log("Your account is not active");
    } else {
      if (balance === 0) {
        console.log("Your account is empty");
      } else {
        console.log("Your balance is negative");
      }
    }
  }
} else {
  console.log("Have a nice day!");
}
