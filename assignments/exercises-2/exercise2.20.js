const args = process.argv.slice(2);
if (args.length < 1) {
  console.log("you need to provide at least 1 argument");
} else if (args.length === 1) {
  const stringReceived = args[0];
  console.log("original string:", stringReceived);
  console.log("last word dropped:", stringReceived.split(" ").slice(0, -1).join(" "));
} else if (args[0].length === 1 && args[1].length === 1 && args.length === 3) {
  const charReplaced = args[0];
  const charReplacing = args[1];
  const stringReceived = args[2];
  console.log(`Original string ${stringReceived}, ${charReplaced}s will be replaced with ${charReplacing}s`);
  // onko search case-insensitive? pitäisikö replacee iso kirjain isolla ja pieni pienellä?
  console.log("->", stringReceived.replace(new RegExp(charReplaced, "gi"), charReplacing));
} else {
  console.log("you can call this program with either 1 or 3 arguments");
  console.log("when called with one, it will drop the last word from it");
  console.log("when called with three, the first 2 arguments need to be characters and the final argument is a string which will be modified by changing instances of the first argument to instances of the second one");
}