import { useState, useEffect } from 'react'
import './App.css'

function App() {
  const [time, setTime] = useState([0, 0])
  const [currentTimer, setCurrentTimer] = useState(null)
  const [timerStopped, setTimerStopped] = useState(false)

  useEffect(() => {
    if (timerStopped) {
      return
    }
    const timer = setTimeout(() => incrementTime(), 1000)
    setCurrentTimer(timer)
    return () => clearTimeout(timer)
  }, [time])

  const incrementTime = () => {
    const [minutes, seconds] = time
    const newSeconds = seconds + 1
    if (newSeconds >= 60) {
      return setTime([minutes + 1, 0])
    }
    setTime([minutes, newSeconds])
  }

  const addSecond = () => {
    incrementTime()
  }

  const addMinute = () => {
    const [minutes, seconds] = time
    setTime([minutes + 1, seconds])
  }

  const stopTimer = () => {
    clearTimeout(currentTimer)
    setTimerStopped(true)
  }

  const startTimer = () => {
    setTimerStopped(false)
    setTime([time[0], time[1]])
  }

  const handleStopAndStart = () => {
    if (!timerStopped) {
      stopTimer()
      return
    }
    startTimer()
  }

  return (
    <div className="App">
      <div className="timer">
        {time[0] < 10 ? '0' : ''}{time[0]}:{time[1] < 10 ? '0' : ''}{time[1]}
      </div>
      <div>
        <button onClick={() => setTime([0, 0])}>reset</button>
        <button onClick={addSecond}>add sec</button>
        <button onClick={addMinute}>add min</button>
        <button onClick={handleStopAndStart}>{!timerStopped ? 'stop' : 'start'}</button>

      </div>
    </div>
  )
}

export default App
