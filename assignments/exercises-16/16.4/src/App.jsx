import { useState } from 'react'
import './App.css'
let id = 1
function App() {
  const [feedbackList, setFeedbackList] = useState([])
  const [type, setType] = useState('')
  const [feedback, setFeedback] = useState('')
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')

  const submitForm = event => {
    event.preventDefault()
    // console.log([type, feedback, name, email].join("-"))
    setFeedbackList(feedbackList.concat({ id: id++, type, feedback, name, email }))

    setType('')
    setFeedback('')
    setName('')
    setEmail('')
  }

  const resetValues = () => {
    setType('')
    setFeedback('')
    setName('')
    setEmail('')
  }

  const isButtonDisabled = () => !(type && feedback)

  return (
    <div className='App'>
      <form onSubmit={submitForm}>
        <div className='row'>
          <label>feedback type: </label>
          feedback <input type="radio" value="feedback" onChange={(event) => setType(event.currentTarget.value)} checked={type === 'feedback'} />
          suggestion <input type="radio" value="suggestion" onChange={(event) => setType(event.currentTarget.value)} checked={type === 'suggestion'} />
          question <input type="radio" value="question" onChange={(event) => setType(event.currentTarget.value)} checked={type === 'question'} />
        </div>
        <div className='row feedback'>
          <label>feedback: </label> 
          <textarea value={feedback} onChange={event => setFeedback(event.target.value)} />
        </div>
        <div className='row'>
          <label>name: </label>
          <input type="text" value={name} onChange={event => setName(event.target.value)} />
        </div>
        <div className='row'>
          <label>email: </label>
          <input type="text" value={email} onChange={event => setEmail(event.target.value)} />
        </div>
      <button type='submit' disabled={isButtonDisabled()} >submit</button>
      </form>
      <button class='resetButton' onClick={resetValues}>reset</button>

      <div className='feedbackList'>
        {feedbackList.length > 0 ? feedbackList.map(feedback => <p key={feedback.id}>{feedback.name || 'John Doe'}({feedback.email || 'unknown' }) - {feedback.type}: {feedback.feedback}</p>)  : 'no feedback received :('}
      </div>
    </div>
  )
}

export default App
