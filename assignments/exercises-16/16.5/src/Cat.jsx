import './Cat.css'

const Cat = ({ image }) => {
  return (
    <>
      { image ? <img src={image} /> : <div>couldn't fetch cat image</div> }
    </>
  )
}

export default Cat