import { useState, useEffect } from 'react'
import './App.css'
import Cat from './Cat'
import Loading from './Loading'

function App() {
  const [loading, setLoading] = useState(false)
  const [image, setImage] = useState(null)
  const [text, setText] = useState('')

  useEffect(() => {
    // fetching twice because react.strictmode + dev server - still keeping it on
    fetchCat()
  }, [])

  const fetchCat = async () => {
    if (loading) {
      return
    }
    setLoading(true)
    const url = text ? `https://cataas.com/c/s/${text}` : 'https://cataas.com/c'
    try {
      const response = await fetch(url)
      // console.log(response)
      const imageBlob = await response.blob()
      const imageUrl = URL.createObjectURL(imageBlob)

      setImage(imageUrl)
    } catch (error) {
      console.log('error', error)
      setImage(null)
    } finally {
      setLoading(false)
    }
  }

  return (
    <div className='App'>
      <div className='cat-image'>
        {loading ? <Loading /> : <Cat image={image} />}
      </div>
      <div className='input'>
        <input type='text' value={text} onChange={event => setText(event.target.value)} />
        <button onClick={fetchCat}>refresh</button>
      </div>
    </div>
  )
}

export default App
