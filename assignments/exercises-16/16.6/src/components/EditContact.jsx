import Form from './Form'
import './EditContact.css'

const EditContact = ({ contact, setSelectedContact, setContacts, setPage }) => {

  return (
    <div className='edit-contact-div'>
      <h1>Edit contact</h1>
      <Form contact={contact} setSelectedContact={setSelectedContact} setContacts={setContacts} setPage={setPage} editing={true}/>
    </div>
  )
}

export default EditContact