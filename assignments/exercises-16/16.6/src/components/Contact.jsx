import './Contact.css'

const Contact = ({ contact, setContacts, setPage, setSelectedContact}) => {
  if (!contact) {
    return (
      <div className='contact-div'>
        No contact selected
      </div>
    )
  }

  const removeContact = id => {
    setContacts(contacts => contacts.filter(contact => contact.id !== id))
    setSelectedContact(null)
    setPage('main')
  }

  const editContact = () => setPage('editContact')

  const { id, name } = contact

  const makeFirstCharOfStringUppercase = string => string[0].toUpperCase() + string.substring(1)

  return (
    <div className='contact-div'>
      <h1>{name}</h1>
      <ul className='contact-ul'>
        {
          Object.entries(contact)
            .filter(([fieldName, _value]) => !(fieldName === 'id' || fieldName === 'name') )
            .filter(([_fieldName, value]) => value)
            .map(([fieldName, value]) => 
              <li 
                key={fieldName} 
                className='contact-li'
              >
                <i>{makeFirstCharOfStringUppercase(fieldName)}: </i>{value}
              </li>
            )
        }
      </ul>
      <div className='contact-buttons-div'>
        <button className='contact-button' onClick={() => removeContact(id)}>Remove</button>
        <button className='contact-button' onClick={editContact}>Edit</button>
      </div>
    </div>
  )
}

export default Contact