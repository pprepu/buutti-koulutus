import { useState } from 'react'
import './ContactList.css'

const ContactList = ({ contacts, setPage, setSelectedContact }) => {

  const [filter, setFilter] = useState('')

  const handleClick = id => {
    const contactFound = contacts.find(contact => contact.id === id)
    if (!contactFound) {
      console.log('strange things happening @ contact-handleclick')
      return
    }

    setSelectedContact(contactFound)
    setPage('contact')
  }

  return (
    <div className='contact-list-div'>
      <input className='filter-input' onChange={(event) => setFilter(event.target.value)} />
      <ul className='contact-list'>
        {filter
          ? contacts
            .filter(contact => contact.name.toLowerCase().includes(filter.toLowerCase()))
            .map(contact =>
              <li className='contact-list-item' key={contact.id} onClick={() => handleClick(contact.id)}>
                {contact.name.length < 18 ? contact.name : contact.name.substring(0, 18)}
              </li>
            )
          : contacts
            .map(contact =>
              <li className='contact-list-item' key={contact.id} onClick={() => handleClick(contact.id)}>
                {contact.name.length < 18 ? contact.name : contact.name.substring(0, 18)}
              </li>
            )
        }
      </ul>
      <button className='contact-list-button' onClick={() => setPage('addContact')}>add contact</button>
    </div>
  )
}

export default ContactList