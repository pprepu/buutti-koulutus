import './Form.css'

import { useState } from 'react'

const Form = ({ contact, setSelectedContact, setContacts, setPage, editing }) => {
  const [name, setName] = useState(contact?.name || '')
  const [email, setEmail] = useState(contact?.email || '')
  const [phone, setPhone] = useState(contact?.phone || '')
  const [address, setAddress] = useState(contact?.address || '')
  const [website, setWebsite] = useState(contact?.website || '')
  const [notes, setNotes] = useState(contact?.notes || '')
  const [formMessage, setFormMessage] = useState([null, ''])

  const editContact = (newContact) => {
    setContacts(contacts => contacts
      .map(contactInArray => contactInArray.id === contact.id
        ? { id: contact.id, ...newContact }
        : contactInArray)
    )
    createFormMessage('Edits saved')
  }

  const addContact = (newContact) => {
    setContacts(contacts => {
      const highestId = Math.max(...contacts.map(contactInArray => contactInArray.id))
      return contacts.concat({ id: highestId + 1, ...newContact })
    })
    createFormMessage('Contact added')
  }

  const onSubmit = event => {
    event.preventDefault()
    if (name.trim() === '') {
      createFormMessage('Name must be given!')
      return
    }
    const newContact = { name, email, phone, address, website, notes }

    if (editing) {
      editContact(newContact)
    } else {
      addContact(newContact);
    }
  }

  const createFormMessage = string => {
    if (formMessage[0]) {
      clearTimeout(formMessage[0])
    }

    const timer = setTimeout(() => setFormMessage([null, '']), 2000)
    setFormMessage([timer, string])
  }

  const goBackToMainPage = () => {
    setSelectedContact(null)
    setPage('main')
  }

  return (
    <div className='form-div'>
      <form className='form' onSubmit={event => onSubmit(event)}>
        <div className='form-row'>
          <label className='form-label'>Name</label>
          <input className='form-input' value={name} onChange={event => setName(event.target.value)} />
        </div>
        <div className='form-row'>
          <label className='form-label'>Email</label>
          <input className='form-input' value={email} onChange={event => setEmail(event.target.value)} />
        </div>
        <div className='form-row'>
          <label className='form-label'>Phone</label>
          <input className='form-input' value={phone} onChange={event => setPhone(event.target.value)} />
        </div>
        <div className='form-row'>
          <label className='form-label'>Address</label>
          <input className='form-input' value={address} onChange={event => setAddress(event.target.value)} />
        </div>
        <div className='form-row'>
          <label className='form-label'>Website</label>
          <input className='form-input' value={website} onChange={event => setWebsite(event.target.value)} />
        </div>
        <div className='form-row'>
          <label className='form-label'>Notes</label>
          <textarea className='form-textarea' value={notes} onChange={event => setNotes(event.target.value)}></textarea>
        </div>
        <div className='form-row form-buttons'>
          <button type='submit' className='form-button'>Save</button>
          <button type='button' className='form-button' onClick={goBackToMainPage}>Cancel</button>
        </div>
      </form>
      <div className='form-message-div'>
        <p className={formMessage[0] ? 'form-message form-message-visible' : 'form-message form-message-invisible'}>{formMessage[1]}</p>
      </div>
    </div>
  )
}

export default Form