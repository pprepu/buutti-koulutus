import Form from './Form'
import './AddContact.css'

const AddContact = ({ contact, setSelectedContact, setContacts, setPage }) => {
  return (
    <div className='add-contact-div'>
      <h1>Add a contact</h1>
      <Form contact={contact} setSelectedContact={setSelectedContact} setContacts={setContacts} setPage={setPage} editing={false}/>
    </div>
  )
}

export default AddContact