import { useState } from 'react'
import './App.css'
import ContactList from './components/ContactList'
import Main from './components/Main'
import AddContact from './components/AddContact'
import EditContact from './components/EditContact'
import Contact from './components/Contact'

const initialContactsForDev = [
  {
    id: 1,
    name: 'Jarno Jönsson',
    email: 'jarno@hotmail.com',
    address: 'wantaa',
    notes: 'kuka haluu luoda lantaa?'
  },
  {
    id: 2,
    name: 'Helmi Pöllö',
    phone: '55578945',
    website: 'helminkotisivu.geocities.com',
    notes: 'helmi tyyppi'
  },
  {
    id: 3,
    name: 'Matti "Masa" Näsä',
    phone: '55547414'
  }
]

const initialContacts = process.env.NODE_ENV === 'development' ? initialContactsForDev : []

function App() {
  const [contacts, setContacts] = useState(initialContacts)
  const [selectedContact, setSelectedContact] = useState(null)
  const [page, setPage] = useState('main')

  return (
    <div className="app">
      <ContactList contacts={contacts} setPage={setPage} setSelectedContact={setSelectedContact} />
      <div className="page">
        {page === 'main' && <Main />}
        {page === 'addContact' &&
          <AddContact
            setContacts={setContacts}
            setPage={setPage}
            setSelectedContact={setSelectedContact}
          />
        }
        {page === 'editContact' &&
          <EditContact
            contact={selectedContact}
            setContacts={setContacts}
            setPage={setPage}
            setSelectedContact={setSelectedContact}
          />
        }
        {page === 'contact' &&
          <Contact
            contact={selectedContact}
            setContacts={setContacts}
            setPage={setPage}
            setSelectedContact={setSelectedContact}
          />}

      </div>
    </div>
  )
}

export default App
