const ErrorText = ({ errorText }) => <div className='error-text'>{errorText}</div>

export default ErrorText