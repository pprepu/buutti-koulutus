const SpecialButton = ({className, buttonText, handleClick}) => {
  return (
    <button className={className} onClick={handleClick}>{buttonText}</button>
  )
}

export default SpecialButton