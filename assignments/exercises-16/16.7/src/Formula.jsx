const Formula = ({ formula }) => {
  return (
    <div className='formula'>
      <p className='formula-text'>
        {formula}
      </p>
    </div>
  )
}

export default Formula