const OperatorButton = ({ className, operation, setFormula, formula }) => {
  
  const replaceLastCharOfFormula = (currentFormula, newChar) => 
    currentFormula.substring(0, currentFormula.length - 1) + newChar

  const getLastCharOfFormula = currentFormula => currentFormula[currentFormula.length - 1]

  const hasOperator = string => new RegExp('[+*/-]').test(string)

  const handleClick = () => {
    if (hasOperator(getLastCharOfFormula(formula))) {
      setFormula(replaceLastCharOfFormula(formula, operation))
      return
    }

    setFormula(current => current + operation)
  }

  return (
    <button className={className} onClick={handleClick}>{operation}</button>
  )
}

export default OperatorButton