const NumberButton = ({ className, number, setFormula }) => {
  const handleClick = () => setFormula(formula => {
    if (formula === '0') {
      return number
    }
    return formula + number
  })
  return (
    <button className={className} onClick={handleClick}>{number}</button>
  )
}

export default NumberButton