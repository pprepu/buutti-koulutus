import { useState } from 'react'
import './App.css'

import ErrorText from './ErrorText'
import Formula from './Formula'

import NumberButton from './NumberButton'
import OperatorButton from './OperatorButton'
import SpecialButton from './SpecialButton'
import CalcButton from './CalcButton'

function App() {
  const [formula, setFormula] = useState('0')
  const [errorText, setErrorText] = useState([null, ''])

  const createErrorText = text => {
    const [currentTimer, _] = errorText
    if (currentTimer) {
      clearTimeout(currentTimer)
    }

    const newTimer = setTimeout(() => setErrorText([null, '']), 2000)
    setErrorText([newTimer, text])
  }

  const hasOperator = string => new RegExp('[+*/-]').test(string)

  const getLastCharOfFormula = currentFormula => currentFormula[currentFormula.length - 1]

  const splitFormulaToPartsAndReturnObject = () => {
    const operators = formula.split('').filter(char => hasOperator(char))
    const operands = formula.split(/[+*/-]/)
    return { operators, operands }
  }

  const handleClickDot = () => {
    const lastCharOfFormula = getLastCharOfFormula(formula)
    const operandArray = splitFormulaToPartsAndReturnObject(formula).operands
    const currentOperand = operandArray[operandArray.length - 1]
    if ('+-*/'.includes(lastCharOfFormula) || currentOperand.includes('.')) {
      return
    }
    setFormula(formula + '.')
  }

  return (
    <div className='App'>
      <ErrorText errorText={errorText[1]} />
      <Formula formula={formula} />
      <div className='calc-grid'>
        <OperatorButton className='addition' operation='+' setFormula={setFormula} formula={formula} />
        <OperatorButton className='subtraction' operation='-' setFormula={setFormula} formula={formula} />
        <OperatorButton className='multiplication' operation='*' setFormula={setFormula} formula={formula} />
        <OperatorButton className='division' operation='/' setFormula={setFormula} formula={formula} />

        <NumberButton className='one' number='1' setFormula={setFormula} />
        <NumberButton className='two' number='2' setFormula={setFormula} />
        <NumberButton className='three' number='3' setFormula={setFormula} />
        <CalcButton
          buttonText='='
          setFormula={setFormula}
          formula={formula}
          createErrorText={createErrorText}
          splitFormulaToPartsAndReturnObject={splitFormulaToPartsAndReturnObject}
          getLastCharOfFormula={getLastCharOfFormula}
        />

        <NumberButton className='four' number='4' setFormula={setFormula} />
        <NumberButton className='five' number='5' setFormula={setFormula} />
        <NumberButton className='six' number='6' setFormula={setFormula} />

        <NumberButton className='seven' number='7' setFormula={setFormula} />
        <NumberButton className='eight' number='8' setFormula={setFormula} />
        <NumberButton className='nine' number='9' setFormula={setFormula} />

        <NumberButton className='zero' number='0' setFormula={setFormula} />
        <SpecialButton className='dot' buttonText='.' handleClick={handleClickDot} />
        <SpecialButton className='clear' buttonText='C' handleClick={() => setFormula('0')} />
      </div>
    </div>
  )
}

export default App