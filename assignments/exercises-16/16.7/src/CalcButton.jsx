const CalcButton = ({buttonText, setFormula, formula, createErrorText, splitFormulaToPartsAndReturnObject, getLastCharOfFormula}) => {

  const calculateTwoValuesWithOperator = (value1, value2, operator) => {
    switch(operator) {
    case '+':
      return value1 + value2
    case '-':
      return value1 - value2
    case '*':
      return value1 * value2
    case '/':
      return value1 / value2
    default:
      return NaN
    }
  }
  
  const splitFormulaByOperators = (formula, operators) => {
    return formula.split(new RegExp(`[${operators}]`))
  }
  
  const filterOperators = (formula, operators) => formula.split('').filter(char => operators.includes(char))

  const calculate = formula => {
    const addAndSubtractOperators = filterOperators(formula, '+-')
    let formulaPartlyCalculated = splitFormulaByOperators(formula, '+-')
      .map(splitFormula => {
        const multiAndDivideOperators = filterOperators(splitFormula, '*/')
        if (multiAndDivideOperators.length === 0) {
          return splitFormula
        }
        const valuesToBeMultipliedOrDivided = splitFormulaByOperators(splitFormula, '*/').map(value => Number(value))
        return multiAndDivideOperators
          .reduce((acc, cur, index) => calculateTwoValuesWithOperator(acc, valuesToBeMultipliedOrDivided[index+1], cur), valuesToBeMultipliedOrDivided[0])
      })
    formulaPartlyCalculated = formulaPartlyCalculated.map(value => Number(value))
    if (formulaPartlyCalculated.length > 1) {
      return addAndSubtractOperators
        .reduce((acc, cur, index) => calculateTwoValuesWithOperator(acc, formulaPartlyCalculated[index+1], cur), formulaPartlyCalculated[0])
    } else {
      return formulaPartlyCalculated[0]
    }
  }

  const handleClick = () => {

    const formulaObject = splitFormulaToPartsAndReturnObject(formula)
    if (formulaObject.operands.length < 2 || formulaObject.operands[1] === '') {
      createErrorText('Please provide at least two operands with an operator')
      return
    }

    if (!/\d/.test(getLastCharOfFormula(formula))) {
      createErrorText('Invalid formula.')
      return
    }
    const calcResult = calculate(formula)
    if (isNaN(calcResult) || calcResult === Infinity || calcResult === -Infinity) {
      createErrorText('Impossible to calculate')
      setFormula('0')
      return
    }

    setFormula(calcResult + '')
  }
  return (
    <button className='equals' onClick={handleClick}>{buttonText}</button>
  )
}

export default CalcButton