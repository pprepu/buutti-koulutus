import { useState } from 'react'
import './App.css'
import Number from './Number'

let id = 1

const HIGHEST_NUMBER = 30

function App() {
  const [remainingNumbers, setRemainingNumbers] = useState(Array(HIGHEST_NUMBER).fill().map((_, i) => i + 1))
  const [numbers, setNumbers] = useState([])


  const pickRandomNumber = () => {
    const randomIndex = Math.floor(Math.random() * remainingNumbers.length)
    return remainingNumbers[randomIndex]
  }
  const addNumber = () => {
    if (remainingNumbers.length === 0) {
      return
    }
    const newNumber = pickRandomNumber()
    setRemainingNumbers(remainingNumbers.filter(number => number !== newNumber))
    setNumbers(numbers.concat(newNumber))
  }

  const resetApp = () => {
    setRemainingNumbers(Array(HIGHEST_NUMBER).fill().map((_, i) => i + 1))
    setNumbers([])
  }

  return (
    <div className="App">
      <h1>Bingo</h1>
      <div className="remainingNumbers">
      {remainingNumbers.length === 0 ? 'no numbers remaining' : remainingNumbers.map(number => <Number key={id++} number={number} notAdded={true} />)}
      </div>
      <div className="numbers">
        {numbers.length === 0 ? 'no numbers picked' : numbers.map(number => <Number key={id++} number={number} />)}
      </div>
      <div className="buttons">
        <button onClick={addNumber}>add</button>
        <button onClick={resetApp}>reset</button>
      </div>
      
    </div>
  )
}

export default App
