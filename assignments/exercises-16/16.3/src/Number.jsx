import './Number.css'

const Number = ({ number, notAdded }) => {
  return (
    <div className={notAdded ? 'number animated' : 'number'}>
      {number}
    </div>
  )
}

export default Number