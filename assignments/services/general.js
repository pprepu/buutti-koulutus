import path from "path";

/**
 * Parses the filename of the running script into a string, removes the extension (.js etc)
 * @param {string} extension of the file being run (optional, defaults to js)
 * @returns {string} the filename as string
 */
const parseCurrentFilename = (extension = "js") => {
  let fileNameWithoutPath = path.parse(process.argv[1]).base;
  const fileNameComponents = fileNameWithoutPath.split(".");
  // whether the user runs the script file with its extension or not 
  // affects the string this function receives.
  //    -- e.g node index.js  VS   node index --
  // therefore, this conditional exists to make sure we are removing 
  // something (the extension) from the string only when something actually needs to be removed
  if (fileNameComponents[fileNameComponents.length - 1] !== extension) {
    return fileNameComponents.join(".");
  }
  return fileNameComponents
    .slice(0, fileNameComponents.length - 1)
    .join(".");
};

export default {
  parseCurrentFilename,
};