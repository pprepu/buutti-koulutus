import fs from "fs";
import generalService from "./general.js";
/**
 * Loads data from a .json file (synchronous) and parses it into a javascript object
 * @param {string} fileName (optional, defaults to current file's filename (without extension))
 * @returns {Object} a javascript object
 */
const loadAndParseJson = (fileName = generalService.parseCurrentFilename()) => {
  try {
    const data = fs.readFileSync(`${fileName}.json`, "utf-8");
    return JSON.parse(data);
  } catch (error) {
    console.error("@services/files.js-loadAndParseJson --- No data found. Filename:", fileName);
  }
};

/**
 * Converts a javascript object into a JSON string and saves it to a .json file (synchronous)
 * @param {Object} object, the data you want to save
 * @param {string} fileName (optional, defaults to current file's filename (without extension))
 */
const stringifyObjectAndSaveAsJson = (object, fileName = generalService.parseCurrentFilename()) => {
  try {
    const json = JSON.stringify(object, null, 2);
    fs.writeFileSync(`${fileName}.json`, json, "utf-8");
  } catch (error) {
    console.error("@services/files.js-stringifyObjectAndSaveAsJson --- Error when trying to save data. Filename:", fileName);
  }
};

export default {
  stringifyObjectAndSaveAsJson,
  loadAndParseJson,
};