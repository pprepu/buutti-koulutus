import express from "express";
import noteRouter from "./noteRouter.js";

const server = express();

server.use(express.json());
server.use("/notes", noteRouter);

export default server;