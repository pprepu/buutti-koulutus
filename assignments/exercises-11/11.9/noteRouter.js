import express from "express";
const router = express.Router();

let notes = [];
const createIdForNote = notes => notes.length > 0
  ? Math.max(...notes.map(note => note.id)) + 1
  : 1;

router.get("/", (req, res) => {
  res.status(200).send(notes);
});

router.get("/:id", (req, res) => {
  const id = Number(req.params.id);
  const note = notes.find(note => note.id === id);
  if (!note) {
    return res.status(404).send("note not found");
  }

  res.status(200).send(note);
});

router.post("/", (req, res) => {
  const { text } = req.body;
  if (!text) {
    return res.status(400).send("no text field found");
  }

  const newNote = {
    text,
    id: createIdForNote(notes)
  };

  notes = notes.concat(newNote);
  res.status(201).send();
});

router.delete("/:id", (req, res) => {
  const id = Number(req.params.id);
  const filteredNotes = notes.filter(note => note.id !== id);
  if (filteredNotes.length === notes.length) {
    return res.status(404).send("note not found");
  }
  notes = filteredNotes;
  res.status(204).send();
});

export default router;