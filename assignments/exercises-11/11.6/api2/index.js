import express from "express";
import axios from "axios";
const getUrl = "http://api1:3000";
const PORT = 3000;

const server = express();

server.get("/", (req, res) => {
  res.send("@api 2");
});

server.get("/testApi2", async (req, res) => {
  const response = await axios.get(getUrl);
  res.send("fetched data " + response.data);
});

server.listen(PORT, () => {
  console.log("listening to port", PORT);
});