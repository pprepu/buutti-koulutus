import express from "express";
const router = express.Router();
let secretNotes = [];
const createIdForNote = secretNotes => secretNotes.length > 0
  ? Math.max(...secretNotes.map(note => note.id)) + 1
  : 1;

router.get("/", (req, res) => {
  res.status(200).send(secretNotes);
});

router.get("/:id", (req, res) => {
  const id = Number(req.params.id);
  const note = secretNotes.find(note => note.id === id);
  if (!note) {
    return res.status(404).send("secret note not found");
  }

  res.status(200).send(note);
});

router.post("/", (req, res) => {
  const { text } = req.body;
  if (!text) {
    return res.status(400).send("no text field found");
  }

  const newNote = {
    text,
    id: createIdForNote(secretNotes)
  };

  secretNotes = secretNotes.concat(newNote);
  res.status(201).send();
});

router.delete("/:id", (req, res) => {
  const id = Number(req.params.id);
  const filteredNotes = secretNotes.filter(note => note.id !== id);
  if (filteredNotes.length === secretNotes.length) {
    return res.status(404).send("secret note not found");
  }
  secretNotes = filteredNotes;
  res.status(204).send();
});

export default router;