import "dotenv/config";
import argon2 from "argon2";

export const authenticate = async (req, res, next) => {
  const { username, password } = req.body;

  if(!username || !password) {
    return res.status(401).send("missing username or password");
  }

  if (username !== process.env.USER_USERNAME) {
    return res.status(401).send("invalid username");
  }

  const match = await argon2.verify(process.env.USER_PASSWORDHASH, password);

  if (!match) {
    return res.status(401).send("invalid password");
  }

  next();
};
