import express from "express";
import noteRouter from "./noteRouter.js";
import secretRouter from "./secretRouter.js";
import { authenticate } from "./middlewares.js";

const server = express();

server.use(express.json());
server.use("/notes", noteRouter);
server.use("/secret", authenticate, secretRouter);

export default server;