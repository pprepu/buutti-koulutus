import server from "./server.js";
const PORT = 3000;

// requests in ./requests folder should either be gitignored or transformed into not spilling secret information
// but I'm leaving them in just in case they help with testing the app

server.listen(PORT, () => {
  console.log("listening to port", PORT);
});