let fibonacciSequence = [];

const wrapValueInPromise = value => new Promise((res, _rej) => {
  setTimeout(() => res(value), 1000);
});

const calcNextNumberInFiboSequence = fibonacciSequence => {
  const nextNumber = fibonacciSequence.length;

  if (nextNumber === 0) {
    return 0;
  }

  if (nextNumber === 1) {
    return 1;
  }

  return fibonacciSequence[nextNumber - 2] + fibonacciSequence[nextNumber - 1];
};

const printFibonacciNumbersWithDelay = async fibonacciSequence => {
  while (true) {
    const nextNumber = await wrapValueInPromise(calcNextNumberInFiboSequence(fibonacciSequence));
    console.log(nextNumber);
    fibonacciSequence = fibonacciSequence.concat(nextNumber);
  }
};

printFibonacciNumbersWithDelay(fibonacciSequence);