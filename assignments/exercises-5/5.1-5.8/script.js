const greet = () => {
  console.log("welcome");
};

let counter = 0;
const count = () => {
  window.alert(`Clicked ${++counter} time${counter < 2 ? "" : "s"}.`);
};

window.onload = greet;