const createListElementWithNameAndPost = (name, post) => {
  const listElement = document.createElement("li");
  
  const nameElement = document.createElement("div");
  nameElement.innerText = name;
  nameElement.classList.add("postName");

  const buttonElement = document.createElement("button");
  buttonElement.innerText = "Delete";
  buttonElement.classList.add("postButton");

  buttonElement.addEventListener("click", () => {
    const postList = document.querySelector(".postList");
    postList.removeChild(listElement);
  });

  const nameAndButtonContainer = document.createElement("div");
  nameAndButtonContainer.classList.add("postNameButtonContainer");
  nameAndButtonContainer.appendChild(nameElement);
  nameAndButtonContainer.appendChild(buttonElement);

  listElement.appendChild(nameAndButtonContainer);

  const postElement = document.createElement("p");
  postElement.innerText = post;
  postElement.classList.add("postText");
  listElement.appendChild(postElement);

  return listElement;
};

const form = document.querySelector("form");
form.addEventListener("submit", event => {
  event.preventDefault();

  const name = event.target.name.value;
  const post = event.target.post.value;

  if (!name || !post) {
    alert("You need to provide both name and post");
    return;
  }

  const postList = document.querySelector(".postList");
  const newPost = createListElementWithNameAndPost(name, post);
  postList.appendChild(newPost);

  event.target.name.value = "";
  event.target.post.value = "";
});

document.querySelector(".toggleButton").addEventListener("click", () => {
  form.classList.toggle("invisible");
});