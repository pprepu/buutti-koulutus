export const validateFields = (req, res, next) => {
  const { title, lyrics } = req.body
  if (!title || !lyrics) {
    return res.status(400).send('fields missing')
  }
  next()
}