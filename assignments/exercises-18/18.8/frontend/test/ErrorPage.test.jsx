import { describe, it, expect, vi } from 'vitest'
import { render, screen } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'

import ErrorPage from '../src/ErrorPage'

describe('ErrorPage, ', () => {
  it('has an h2-element with text "404 - Not found" when useRouteError returns an object with a property "status" and value "404"', () => {
    vi.mock('react-router-dom', async () => {
      const normalImports = await vi.importActual('react-router-dom')
      return {
        ...normalImports,
        useRouteError: vi.fn(() => ({ status: 404 }))
      }
    })
    render(
      <MemoryRouter>
        <ErrorPage />
      </MemoryRouter>
    )
    const h2Element = screen.getByRole('heading', { level: 2 })
    expect(h2Element).toHaveTextContent('404 - Not Found')
  })
})