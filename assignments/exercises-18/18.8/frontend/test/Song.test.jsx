import { describe, it, expect, vi } from 'vitest'
import { render, screen, waitFor, act } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'

import Song from '../src/Song'

const testSong = { id: 1, title: "test song", lyrics: "verse1\n\nverse2" }

describe('Song, ', () => {
  beforeAll(() => {
    vi.mock('react-router-dom', async () => {
      const normalImports = await vi.importActual('react-router-dom')
      return {
        ...normalImports,
        useLoaderData: vi.fn(() => testSong.id)
      }
    })
  }),

    beforeEach(() => {
      fetch.once(JSON.stringify(testSong))
    }),

    it('renders song title as an h2 element according to the data it fetches from backend (mocked),', async () => {

      await act(async () => {
        render(
          <MemoryRouter>
            <Song />
          </MemoryRouter>
        )
      })
      await waitFor(() => {
        const h2Element = screen.getByRole('heading', { level: 2 })
        expect(h2Element).toHaveTextContent(testSong.title)
      })
    })

})