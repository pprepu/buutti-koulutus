import { describe, it, expect, vi } from 'vitest'
import { render, screen } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import userEvent from '@testing-library/user-event'

import SongList from '../src/SongList'

// using a hardcoded list of songs for testing
import testSongs from '../src/songs'

// SongList checks if user has selected a song by using useRouteLoaderData function from react-router-dom
// this is the value used for my mock version of that function
const idOfSelectedSong = '1'

describe('SongList, ', () => {

  beforeAll(() => {
    vi.mock('react-router-dom', async () => {
      const normalImports = await vi.importActual('react-router-dom')
      return {
        ...normalImports,
        useRouteLoaderData: vi.fn(() => ({ id: idOfSelectedSong }))
      }
    })
  })
  it('renders no list items, when there are no songs in the app', () => {
    render(
      <MemoryRouter>
        <SongList songs={[]} />
      </MemoryRouter>
    )
    const listitemElements = screen.queryAllByRole('listitem')
    expect(listitemElements).toHaveLength(0)
  }),
  it('renders the matching song in the list as bolded,when a song has been selected in the app', () => {
    render(
      <MemoryRouter>
        <SongList songs={testSongs} />
      </MemoryRouter>
    )
    const songWithSelectedId = testSongs.find(song => song.id === Number(idOfSelectedSong))
    const elementWithSelectedSong = screen.getByText(songWithSelectedId.title)

    expect(elementWithSelectedSong).toHaveClass('bold')
  }),
  it('changes the amount of list items rendered, when user types inside the input field ', async () => {
    render(
      <MemoryRouter>
        <SongList songs={testSongs} />
      </MemoryRouter>
    )

    const listitemElementsBeforeTyping = screen.getAllByRole('listitem')
    
    const user = userEvent.setup()
    const input = screen.getByRole('textbox')
    const typedText = 'hoosianna'
    await user.type(input, typedText)

    const listitemElementsAfterTyping = screen.getAllByRole('listitem')

    expect(listitemElementsBeforeTyping).toHaveLength(testSongs.length)
    expect(listitemElementsAfterTyping).toHaveLength(2) // hardcoded

    const testSongsFilteredWithTypedText = testSongs.filter(song => song.title.toLowerCase().includes(typedText.toLowerCase()))
    expect(listitemElementsAfterTyping).toHaveLength(testSongsFilteredWithTypedText.length) // dynamic

  })
})