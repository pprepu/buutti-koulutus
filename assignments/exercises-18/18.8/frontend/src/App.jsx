import './App.css'

import { Outlet } from 'react-router-dom'
import { useEffect, useState } from 'react'

import SongList from './SongList'

export const urlForSongs = '/songs'

function App() {

  const [songs, setSongs] = useState([])

  const fetchSongs = async () => {
    const response = await fetch(urlForSongs)
    const songsFetched = await response.json()
    setSongs(songsFetched)
  }

  useEffect(() => {
    fetchSongs()
  }, [])
  
  return (
    <div className='app-page-container'>
      <h1>Song book</h1>
      <div className="app-page">
      
      <div className='app-left-column'>
        <SongList songs={songs}/>
      </div>
      <div className='app-right-column'>
        <Outlet />
      </div>
    </div>
    </div>
    
  )
}

export default App