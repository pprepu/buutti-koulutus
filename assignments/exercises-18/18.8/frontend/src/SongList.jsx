import './SongList.css'
import { Link, useRouteLoaderData } from 'react-router-dom'
import { useState } from 'react'

const SongList = ({ songs }) => {
  const [searchString, setSearchString] = useState('')
  const currentSong = useRouteLoaderData('song')

  const handleInputForSearch = event => setSearchString(event.target.value)
  return (
    <ul className='song-list'>
      <h2>Songs</h2>
      <input className='song-search-input' value={searchString} onChange={handleInputForSearch} />
      {songs
        .filter(song => song.title.toLowerCase().includes(searchString.toLowerCase()))
        .map(song => <li key={song.id} className='song-list-item'>
          <Link className='song-list-link 'to={'/' + song.id}>
            {Number(currentSong?.id) !== song.id ? song.title : <b className='bold'>{song.title}</b>}
          </Link>
        </li>
        )
      }
    </ul>
  )
}

export default SongList