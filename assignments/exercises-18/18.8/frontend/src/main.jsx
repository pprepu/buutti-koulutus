import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Song, { loader as songLoader } from './Song'
import ErrorPage from './ErrorPage'

export const router = createBrowserRouter([
  {
      path: '/',
      element: <App />,
      children: [
        {
            path: ':id',
            element: <Song />,
            loader: songLoader,
            id: 'song'
        },
    ],
    errorElement: <ErrorPage />,
  }
])


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
