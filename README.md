# buutti-koulutus

Exercises are in the ./assignments folder.

First week's git assignments are [in another repository](https://gitlab.com/pprepu/gitassignments).

Second week's gitlab CI pipeline is also [in another repository](https://gitlab.com/pprepu/gitlab-pipeline).

Fourth week's repos with CI pipelines: [calculator](https://gitlab.com/pprepu/calculator) and [converter](https://gitlab.com/pprepu/converter).

Seventh week's repos with CI deployment pipelines: [productsAPI](https://gitlab.com/pprepu/productsapi) and [musicAPI](https://gitlab.com/pprepu/musicapi).
